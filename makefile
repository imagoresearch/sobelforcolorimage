SOURCES=Sobel_Test_Color_WeightedGray_3x3.cpp Sobel_function_Weighted_Gray_3x3.cpp

OBJECTS=$(SOURCES:.cpp=.o)


all:
	g++ -std=c++11 -o testSobel Sobel_Test_Color_WeightedGray_3x3.cpp Sobel_function_Weighted_Gray_3x3.cpp -O2 -Wall -v -lpthread -lIMFilters -lfreeimage -lippi -lipps -lippvm -lippcc -lippdc -lippch -lippcore -I. -I/usr/include -I/usr/local/include -I/opt/intel/ipp/include -I/Users/anna.yang/Documents/Develop/AIl_INTEGRATED/IMFilters/include -L/usr/local/lib -L/opt/intel/ipp/lib -L/Users/anna.yang/Documents/Develop/AIl_INTEGRATED/lib
