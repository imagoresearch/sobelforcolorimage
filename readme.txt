1. You need to link to the following libraries in your makefile:
Freeimage: -lfreeimage
IPP: -lippi -lipps -lippvm -lippcc -lippdc -lippch -lippcore
libIMFilters: -lIMFilters

2. You may need to modify the linking path in the makefile according to your environment settings.