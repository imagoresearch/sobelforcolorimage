#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//using namespace cimg_library;

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
//#include <iostream.h>
#include <string.h>
//#include <iomanip.h>
//#include <fstream.h>
#include <assert.h>
//
#include "image.h"

///////////////////////////////////////////////////////////////////
#define nLarge 1000000
#define fLarge 1.0E+10 //6

#define feps 0.000001
#define pi 3.14159

#define PRINTING_SUBIMAGE

#define DEACTIVATE_PRINTING

#define fDeviationFrom_100percentsMax 1.0
////////////////////////////////////////////////////////////////////////////

//The length is from the left to the right; the width is from down to up

#define nLenMin 200 //400 //nLenMax
#define nWidMin 100  // < nWidMax

#define nLenMax 3500 //500 //1300 
#define nWidMax 5500 //4800 //400 
#define nImageSizeMax (nLenMax*nWidMax)

///////////////////////////////////////////////////////////////////////////
#define nLenSubimageToPrintMin 0 //342 
#define nLenSubimageToPrintMax 500 //426

#define nWidSubimageToPrintMin 0 //814 
#define nWidSubimageToPrintMax 882


//////////////////////////////////////////////////////////////////
//statistics
#define nIntensityStatMin 10 //85 

#define nIntensityStatMax 255 

#define nIntensityThresholdForDense 150  // > nIntensityStatMin) and < nIntensityStatMax

#define nNumOfHistogramBinsStat 17

#define nGrayLevelIntensityToDrawContours 100 //40

#define nGrayLevelIntensityForDenseArea 140

//////////////////////////////////////////////////////////////////
typedef struct
{
	int nIndicOfClass;

	int nLenCur; //<= nLenMax
	int nWidCur; //<= nWidMax

	//int nAreaForStat;
	//int nAreaDense;

	//float fPercentageOfobjectArea;

	//int nPixelArr[nImageSizeMax];
	int *nPixelArr;

} ONE_ORIG_IMAGE;

typedef struct
{
bool bUSE_SUMS_OF_IMAGE_GRADIENTSf;

bool bVERSION_2_OF_SOBEL_3x3f;

bool bUSING_ONLY_BLACK_AND_WHITE_COLORSf;

bool bBLACK_BACKGROUND_WHITE_CONTOURSf;

int nIntensityThresholdForSobelMinf; // = 169;
int nIntensityThresholdForSobelMaxf; // = 170;

float fFadingFactorf;

float fWeight_For_2nd_Versionf;

float fNormalizingf;

float fWeight_Redf;
float fWeight_Greenf;
float fWeight_Bluef;

} PARAMETERS_WEIGHTED_GRAY_3x3;

