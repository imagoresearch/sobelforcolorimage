//
//  ParametersConfig.h
//  Density
//
//  Created by Anna Yang on 4/24/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#ifndef ParametersConfig_h
#define ParametersConfig_h

#include <fstream>
#include <sstream>
#include <string>
using namespace std;

class ParameterConfig {
    
public:
    ParameterConfig(string fileName){
        bool status = read(fileName);
        if (status == false) throw -1;
    };
    ~ParameterConfig(){};
    bool bUSE_SUMS_OF_IMAGE_GRADIENTSf;
    
    bool bVERSION_2_OF_SOBEL_3x3f;
    
    bool bUSING_ONLY_BLACK_AND_WHITE_COLORSf;
    
    bool bBLACK_BACKGROUND_WHITE_CONTOURSf;
    
    int nIntensityThresholdForSobelMinf;
    int nIntensityThresholdForSobelMaxf;
    
    float fFadingFactor;
    float fWeight_Red;
    float fWeight_Green;
    float fWeight_Blue;
    float fNormalizing;
    float fWeight_For_3x3_2nd_Version;
    
private:
    bool read(string fileName){
        ifstream fin(fileName);
        string line;
        while (getline(fin, line))
        {
            istringstream in(line);
            string name;
            in >> name;
            if (name == "bUSE_SUMS_OF_IMAGE_GRADIENTS"){
                in >> bUSE_SUMS_OF_IMAGE_GRADIENTSf;
            }
            if (name == "bVERSION_2_OF_SOBEL_3x3"){
                in >> bVERSION_2_OF_SOBEL_3x3f;
            }
            if (name == "bUSING_ONLY_BLACK_AND_WHITE_COLORS"){
                in >> bUSING_ONLY_BLACK_AND_WHITE_COLORSf;
            }
            if (name == "bBLACK_BACKGROUND_WHITE_CONTOURS"){
                in >> bBLACK_BACKGROUND_WHITE_CONTOURSf;
            }
            if (name == "nIntensityThresholdForSobelMin"){
                in >> nIntensityThresholdForSobelMinf;
            }
            if (name == "nIntensityThresholdForSobelMax"){
                in >> nIntensityThresholdForSobelMaxf;
            }
            if (name == "fFadingFactor"){
                in >> fFadingFactor;
            }
            if (name == "fWeight_For_2nd_Version"){
                in >> fWeight_For_3x3_2nd_Version;
            }
            if (name == "fNormalizing"){
                in >> fNormalizing;
            }
            if (name == "fWeight_Red"){
                in >> fWeight_Red;
            }
            if (name == "fWeight_Green"){
                in >> fWeight_Green;
            }
            if (name == "fWeight_Blue"){
                in >> fWeight_Blue;
            }
        }
        return true;
    };
};
#endif /* ParametersConfig_h */
