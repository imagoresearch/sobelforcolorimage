
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

#include "Sobel_function.h"
#include "SobelParametersConfig.h"
#include "fileUtil.h"
#include <vector>
#include <iostream>

using namespace std;

int main(int argc, const char * argv[])
{
	int
		nRes;
	
	//const char inputFileName[] = "ButterFly_Test_Image.jpg";
	//const char outputFileName[] = "ImageSaved_Gray.png";

    ParameterConfig config("sobel_parameters.txt");
    
    bool bBLACK_BACKGROUND_WHITE_CONTOURS = config.bBLACK_BACKGROUND_WHITE_CONTOURSf;
    
    
    //#define USE_SUMS_OF_IMAGE_GRADIENTS
    bool bUSE_SUMS_OF_IMAGE_GRADIENTS = config.bUSE_SUMS_OF_IMAGE_GRADIENTSf;
    
    //#define DIM3x3_SOBEL_FILTER // not together with #define DIM5x5_SOBEL_FILTER
    //bool bDIM3x3_SOBEL_FILTER = true;
    
    //#define DIM5x5_SOBEL_FILTER
    bool bVERSION_2_OF_SOBEL_3x3 = config.bVERSION_2_OF_SOBEL_3x3f;
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    int nIntensityThresholdForSobelMin = config.nIntensityThresholdForSobelMinf;
    int nIntensityThresholdForSobelMax = config.nIntensityThresholdForSobelMaxf;
    
    float fFadingFactor = config.fFadingFactor;
    
    //the weights are in percents
    float fWeight_Red = config.fWeight_Red; //(100.0) //(100.0/3.0)
    
    
    float fWeight_Green = config.fWeight_Green; //20.0 //(100.0/2.0)
    
    float fWeight_Blue = config.fWeight_Blue; //(100.0/2.0)
    
    float fWeight_For_2nd_Version = config.fWeight_For_3x3_2nd_Version; //1.0 //0.5 //0.8 //32.0
    
    float fNormalizing = config.fNormalizing;
    //////////////////////////////////////////////////////////////////
    
    //#define USING_ONLY_BLACK_AND_WHITE_COLORS
    bool bUSING_ONLY_BLACK_AND_WHITE_COLORS = config.bUSING_ONLY_BLACK_AND_WHITE_COLORSf;
    
    vector<string> files;
    string pattern = "/*.{png,jpg,bmp}";
    collectFiles(argv[1], pattern, files);
    string outDirectory = argv[2];
    if (files.size() == 0)
    {
        cout << "No images are found in directory: " << argv[1] << endl;
    }
    
    for (int iFile = 0; iFile < files.size(); iFile++)
    {
        size_t pos = files[iFile].find_last_of("/");
        string imageName = files[iFile].substr(pos+1);
        size_t dotpos = imageName.find_last_of(".");
        string outputFileName = outDirectory + "/" + imageName.insert(dotpos, "_result");
        
        PARAMETERS_WEIGHTED_GRAY_3x3 sParameters_Weighted_Gray_3x3;

        sParameters_Weighted_Gray_3x3.bUSE_SUMS_OF_IMAGE_GRADIENTSf = bUSE_SUMS_OF_IMAGE_GRADIENTS;

        sParameters_Weighted_Gray_3x3.bUSING_ONLY_BLACK_AND_WHITE_COLORSf = bUSING_ONLY_BLACK_AND_WHITE_COLORS;

        sParameters_Weighted_Gray_3x3.bVERSION_2_OF_SOBEL_3x3f = bVERSION_2_OF_SOBEL_3x3;

        sParameters_Weighted_Gray_3x3.nIntensityThresholdForSobelMinf = nIntensityThresholdForSobelMin;
        sParameters_Weighted_Gray_3x3.nIntensityThresholdForSobelMaxf = nIntensityThresholdForSobelMax;

        sParameters_Weighted_Gray_3x3.fFadingFactorf = fFadingFactor;
        sParameters_Weighted_Gray_3x3.fNormalizingf = fNormalizing;

        sParameters_Weighted_Gray_3x3.fWeight_Redf = fWeight_Red;
        sParameters_Weighted_Gray_3x3.fWeight_Greenf = fWeight_Green;
        sParameters_Weighted_Gray_3x3.fWeight_Bluef = fWeight_Blue;

        sParameters_Weighted_Gray_3x3.fWeight_For_2nd_Versionf = fWeight_For_2nd_Version;

        nRes = doSobel_Weighted_Gray_3x3(
                    files[iFile].c_str(), //const char *inputFileNamef,

                        outputFileName.c_str(),	//const char *outputFileNamef,

            &sParameters_Weighted_Gray_3x3); // const PARAMETERS_WEIGHTED_GRAY_3x3 *sParameters_Weighted_Gray_3x3f); // const float fWeight_Bluef)
    }
	
	return 0;
} //int main()
  
  //printf("\n\n Please press any key:"); getchar();
