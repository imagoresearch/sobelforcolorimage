//
//  fileUtil.h
//  Density
//
//  Created by Anna Yang on 4/24/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#ifndef fileUtil_h
#define fileUtil_h

#ifdef _WIN64
    #include <windows.h>
#else
    #include "glob.h"
    #include <dirent.h>
    #include <sys/stat.h>
#endif

#include <stdio.h>
#include <vector>
using namespace std;

void collectFiles(string name, string pattern, vector<string>& files){
#ifdef _WIN32
    // This is where I will hold file details temporarily
    WIN32_FIND_DATA file;
    
    // This will store return value of the FindFirstFile()
    HANDLE fileHandle;
    
    // Give wildcard like e:\someFolder\*
    string searchPattern = name + "\*";
    
    // Call a C++ function to get files in the directory
    fileHandle = FindFirstFile(searchPattern.c_str(), &file);
    if(fileHandle == INVALID_HANDLE_VALUE)
    {
        cout << "Cannot open folder " << name << endl;
    }
    else
    {
        do
        {
            // If this which we found now, is a directory, recursively
            // call the function again
            if((file.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0
               && file.data.cFileName != "."
               && file.data.cFileName != "..")
            {
                // Call our function again to search in this sub-directory
                collectFiles(file.cFileName);
            }
            else            // It is a file
            {
                string fileName = string(file.cFileName);
                size_t dotpos = fileName.find_last_of(".");
                if (fileName.substr(dotpos+1) == "png" || fileName.substr(dotpos+1) == "jpg" || fileName.substr(dotpos+1) == "bmp")
                {
                    string path = name + "/" + fileName;
                    files.push_back(path);
                }
            }
            // Go on and find next file(s) in current dir
        } while(FindNextFile(fileHandle ,&file));
        
        FindClose(fileHandle);
    }
#else
    DIR *dir;
    struct dirent *entry;
    
    if (!(dir = opendir(name.c_str())))
        throw "Cannot open folder!";
    
    glob_t glob_result;
    string searchPattern = name + pattern;
    glob(searchPattern.c_str(), GLOB_BRACE, NULL, &glob_result);
    for(size_t i = 0; i < glob_result.gl_pathc; ++i) {
        files.push_back(string(glob_result.gl_pathv[i]));
    }
    
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            
            string path = name + "/" + string(entry->d_name);
            collectFiles(path, pattern, files);
        }
    }
    closedir(dir);
#endif
}

#endif /* fileUtil_h */
