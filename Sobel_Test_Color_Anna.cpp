

#include "ParametersConfig.h"
#include "Sobel_Test_Color_2_Anna.h"
#include "image.h"
#include "fileUtil.h"
#include <iostream>
#include <vector>

using namespace imago;

int Initializing_One_Orig_Image_To_CurSize(
                                           const int nImageWidthf,
                                           const int nImageLengthf,
                                           
                                           ONE_ORIG_IMAGE *sOne_Orig_Imagef);    //[]

int HSI_Colors(
               const ONE_ORIG_IMAGE *sOne_Orig_Image_Redf,
               const ONE_ORIG_IMAGE *sOne_Orig_Image_Greenf,
               const ONE_ORIG_IMAGE *sOne_Orig_Image_Bluef,
               
               ONE_ORIG_IMAGE *sOne_Orig_Image_Intensityf,
               ONE_ORIG_IMAGE *sOne_Orig_Image_Saturationf,
               ONE_ORIG_IMAGE *sOne_Orig_Image_Huef);

int Histogram_Statistics_For_Orig_Image(
                                        
                                        ONE_ORIG_IMAGE *sOne_Orig_Imagef, //[nImageSizeMax]
                                        
                                        int &nObjectAreaf,
                                        float &fPercentageOfobjectAreaf,
                                        int nHistogramArrf[], //[nNumOfHistogramBinsStat]
                                        float fPercentagesInHistogramArrf[]);//[nNumOfHistogramBinsStat]

int Sobel_Filtering(
                    const ONE_ORIG_IMAGE *sOne_Orig_Imagef,
                    ONE_ORIG_IMAGE *sOne_Filtered_Imagef);

int
iIter_Glob;

FILE *fout;

bool INCLUDE_HSI = false;
bool INCLUDE_WEIGHTED_GRAY = false;
bool USE_SUMS_OF_IMAGE_GRADIENTS = false;
bool SOBEL_FILTER_SIZE_3 = false;
bool SOBEL_FILTER_SIZE_5 = false;
bool SOBEL_VERSION2_3 = false;
bool SOBEL_VERSION2_5 = false;
bool USING_ONLY_BLACK_AND_WHITE_COLORS = false;
bool BLACK_BACKGROUND_WHITE_CONTOURS = false;
bool HUE_BY_1ST_WAY = false;
bool HUE_BY_2ND_WAY = false;
bool CHOOSE_SATURATION = false;
bool CHOOSE_INTENSITY = false;
bool CHOOSE_HUE = false;
float fWeight_For_5x5_Sobel_Filter = 10.0;
float fFadingFactor;
float fWeight_Red;
float fWeight_Green;
float fWeight_Blue;
float fNormalizing;

float fWeight_For_2nd_Version = 2.3;
int main(int argc, const char * argv[])
{
    if (argc < 3){
        printf("Please provide path to input file and path to output file!");
        exit(1);
    }
    vector<string> inDirs;
    vector<string> outDirs;
    inDirs.push_back(argv[1]);
    string outDirectory = argv[2];
    listdir(argv[1], inDirs);
    vector<string> files;
    for (int i = 0; i < inDirs.size(); i++){
        string inFile(inDirs[i]);
        vector<string> pngfiles = getFilePaths(inFile+"/*.png");
        vector<string> jpgfiles = getFilePaths(inFile+"/*.jpg");
        vector<string> bmpfiles = getFilePaths(inFile+"/*.bmp");
        if (pngfiles.size() > 0) files.insert(files.end(), pngfiles.begin(), pngfiles.end());
        if (bmpfiles.size() > 0) files.insert(files.end(), bmpfiles.begin(), bmpfiles.end());
        if (jpgfiles.size() > 0) files.insert(files.end(), jpgfiles.begin(), jpgfiles.end());
    }
    
    ParameterConfig config("contour_parameters.txt");
    INCLUDE_HSI = config.enable_hsi;
    INCLUDE_WEIGHTED_GRAY = config.enable_weighted_gray;
    USING_ONLY_BLACK_AND_WHITE_COLORS = config.using_only_black_and_white_colors;
    BLACK_BACKGROUND_WHITE_CONTOURS = config.black_background_white_contours;
    USE_SUMS_OF_IMAGE_GRADIENTS = config.use_sum_gradients;
    fWeight_For_5x5_Sobel_Filter = config.fWeight_For_5x5_Sobel_Filter;
    fFadingFactor = config.fFadingFactor;
    fWeight_Red = config.fWeight_Red;
    fWeight_Green = config.fWeight_Green;
    fWeight_Blue = config.fWeight_Blue;
    fNormalizing = config.fNormalizing;
    if (config.choose_channel == 0)CHOOSE_HUE = true;
    else if (config.choose_channel == 1)CHOOSE_SATURATION = true;
    else if (config.choose_channel == 2)CHOOSE_INTENSITY = true;
    if (config.choose_channel == 0){
        if (config.hue_method == 1)HUE_BY_1ST_WAY = true;
        else if (config.hue_method == 2)HUE_BY_2ND_WAY = true;
    }
    if (config.sobel_mask_size == 3){
        SOBEL_FILTER_SIZE_3 = true;
        if (config.sobel_mask_version == 2)SOBEL_VERSION2_3 = true;
    }
    else if (config.sobel_mask_size == 5){
        SOBEL_FILTER_SIZE_5 = true;
        if (config.sobel_mask_version == 2)SOBEL_VERSION2_5 = true;
    }

	int 
		iWid,
		iLen,
			//nIndexMaxSize,
		////////////////////////////////////////////////////////
		nImageWidth, // is actually 'nImageLength' comparing with the "Cimage" implementation.
		nImageLength,
		//nImageHeight,

		nObjectArea,
		nHistogramArr[nNumOfHistogramBinsStat], //[nNumOfHistogramBinsStat]

		iHistogramBin,

		nIndexCurSize,
		nNumOfPixelsAboveIntensityThreshold = 0,
		nRes;

	float
		fSumOfPercentsf,
		fDeviationOfSumFrom_100percentsf,

		fPercentageOfobjectArea,
		fPercentagesInHistogramArr[nNumOfHistogramBinsStat]; //[nNumOfHistogramBinsStat]

	fout = fopen("wMain_Anna_Sobel_Test_Color.txt", "w");

	if (fout == NULL)
	{
		printf("\n\n fout == NULL");
		exit(1);
	} //if (fout == NULL)


	  //////////////////////////////////////////////////////////////////////////
for (int iFile = 0; iFile < files.size(); iFile++){
	ONE_ORIG_IMAGE
		sOne_Orig_Image_WeightedGray,


		sOne_Orig_Image_Intensity,
		sOne_Orig_Image_Saturation,
		sOne_Orig_Image_Hue,


		sOne_Orig_Image_Red,
		sOne_Orig_Image_Green,
		sOne_Orig_Image_Blue;

    
	//////////////////////////////////////////////////////////////////////////////////////////////
	//#ifdef INCLUDE_WEIGHTED_GRAY
	fSumOfPercentsf = fWeight_Red + fWeight_Green + fWeight_Blue;
	fDeviationOfSumFrom_100percentsf = 100.0 - fSumOfPercentsf;

	if (fDeviationOfSumFrom_100percentsf < 0.0)
		fDeviationOfSumFrom_100percentsf = -fDeviationOfSumFrom_100percentsf;

	if (fDeviationOfSumFrom_100percentsf > fDeviationFrom_100percentsMax)
	{
		printf("\n\n An error: the sum of the color weights deviates too much from 100 percents");
		printf("\n\n Please adjust the color weights");

		printf("\n\n Please press any key to exit:");
		exit(1);
	} //if (fDeviationOfSumFrom_100percentsf > fDeviationFrom_100percentsMax)

	  //} //#ifdef INCLUDE_WEIGHTED_GRAY

	  ///////////////////////////////////////////////////////////////////////////

	Image testImg;

	testImg.read(files[iFile]);

	// size of image
	//nImageWidth = testImg.width();
	//nImageHeight = testImg.height();

	 nImageWidth = testImg.height();  
	nImageLength = testImg.width();

	printf("\n nImageWidth = %d, nImageLength = %d", nImageWidth, nImageLength);

	if (nImageLength > nLenMax || nImageLength < nLenMin)
	{
		printf("\n\n An error in reading the image width: nImageLength = %d", nImageLength);
		//printf("\n\n Please press any key to exit");
		//getchar();
        exit(1);

	} //if (nImageLength > nLenMax || nImageLength < nLenMin)

	if (nImageWidth > nWidMax || nImageWidth < nWidMin)
	{
		printf("\n\n An error in reading the image height: nImageWidth = %d", nImageWidth);
		//printf("\n\n Please press any key to exit");
		//getchar();
        exit(1);

	} // if (nImageWidth > nWidMax || nImageWidth < nWidMin)

	//printf("\n\n Please press any key:"); getchar();

	if (nIntensityThresholdForDense <= nIntensityStatMin || nIntensityThresholdForDense >= nIntensityStatMax || nIntensityThresholdForDense < 1 || nIntensityStatMin < 1)
	{
		printf("\n\n An error in specification of intensity thresholds: nIntensityThresholdForDense = %d, nIntensityStatMin = %d",
			nIntensityThresholdForDense, nIntensityStatMin);

		printf("\n\n The intensity threshold for the dense area 'nIntensityThresholdForDense' should be larger than the threshold for the object area 'nIntensityStatMin'");

		//printf("\n\n Please press any key to exit:");
		//getchar();
        exit(1);
	} // if (nIntensityThresholdForDense <= nIntensityStatMin || nIntensityThresholdForDense >= nIntensityStatMax)

	//////////////////////////////////////////////////////////////////////////

	//initializing pixels of the maximum possible size to unreasonable values
/*
	nRes = Initializing_One_Orig_Image_To_Max(

		&sOne_Orig_Image_WeightedGray); // ONE_ORIG_IMAGE *sOne_Orig_Imagef) //[nImageSizeMax]
*/
	nRes = Initializing_One_Orig_Image_To_CurSize(
		nImageWidth, //const int nImageWidthf,
		nImageLength, //const int nImageLengthf,

		&sOne_Orig_Image_WeightedGray); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]

/////////////////////////////////////////////////////////////////////////////////

	nRes = Initializing_One_Orig_Image_To_CurSize(
		nImageWidth, //const int nImageWidthf,
		nImageLength, //const int nImageLengthf,

		&sOne_Orig_Image_Red); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]

/////////////////////////////////////////////////////////////////////////////////
	nRes = Initializing_One_Orig_Image_To_CurSize(
		nImageWidth, //const int nImageWidthf,
		nImageLength, //const int nImageLengthf,

		&sOne_Orig_Image_Green); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]

	/////////////////////////////////////////////////////////////////////////////////
	nRes = Initializing_One_Orig_Image_To_CurSize(
		nImageWidth, //const int nImageWidthf,
		nImageLength, //const int nImageLengthf,

		&sOne_Orig_Image_Blue); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]
									
	///////////////////////////////////////////////////////////////////////////////////////

	// use 'nIntensityThreshold' to change the number of object pixels!

	int
		nIntensity_Read_Test_ImageMax = -nLarge,
		nRed,
		nGreen,
		nBlue;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (iWid = 0; iWid < nImageWidth; iWid++) //j
	{
		for (iLen = 0; iLen < nImageLength; iLen++) //i
		{

			//nIndexCurSize = i + iWid*nImageLength;

			nRed = testImg(iWid, iLen, R);

			nGreen = testImg(iWid, iLen, G);
			nBlue = testImg(iWid, iLen, B);

			if (nRed < 0 || nRed > nIntensityStatMax || nGreen < 0 || nGreen > nIntensityStatMax || nBlue < 0 || nBlue > nIntensityStatMax)
			{
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, iWid = %d, iLen = %d", nRed, nGreen, nBlue, iWid,iLen);
				//printf("\n\n Please press any key to exit");
				//getchar();
                exit(1);
			} // if (nRed < 0 || nRed > nIntensityStatMax || nGreen < 0 || nGreen > nIntensityStatMax || nBlue < 0 || nBlue > nIntensityStatMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

			//fprintf(fout, "\n nPixelArr[%d] = %d, iLen = %d, iWid = %d", nIndexOfPixel, nPixelArr[nIndexOfPixel], iLen, iWid);

		} // for (iLen = 0; iLen < nImageLength; iLen++)

	}//for (iWid = 0; iWid < nImageWidth; iWid++)

	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);

	fprintf(fout, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	////////////////////////////////////////////////////////////////////////////////////////////

	for (iWid = 0; iWid < nImageWidth; iWid++) //j
	{
		for (iLen = 0; iLen < nImageLength; iLen++) //i
		{

			nIndexCurSize = iLen + iWid*nImageLength;

			nRed = testImg(iWid, iLen, R);
			nGreen = testImg(iWid, iLen, G);
			nBlue = testImg(iWid, iLen, B);
	
	
			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling
				nRed = nRed / 256;

				nGreen = nGreen / 256;
				nBlue = nBlue / 256;
			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
	

			sOne_Orig_Image_Red.nPixelArr[nIndexCurSize] = (int)(nRed*fFadingFactor);
			sOne_Orig_Image_Green.nPixelArr[nIndexCurSize] = (int)(nGreen*fFadingFactor);
			sOne_Orig_Image_Blue.nPixelArr[nIndexCurSize] = (int)(nBlue*fFadingFactor);

			 //reassigning
			//sOne_Orig_Image_WeightedGray.nPixelArr[nIndexMaxSize] = 
					//(sOne_Orig_Image_Red.nPixelArr[nIndexMaxSize] + sOne_Orig_Image_Green.nPixelArr[nIndexMaxSize] + sOne_Orig_Image_Blue.nPixelArr[nIndexMaxSize]) / 3;

			  //Converting to decimals
			sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize] = (int)(fFadingFactor*(fWeight_Red*(float)(sOne_Orig_Image_Red.nPixelArr[nIndexCurSize]) +
				fWeight_Green*(float)(sOne_Orig_Image_Green.nPixelArr[nIndexCurSize]) +
				fWeight_Blue*(float)(sOne_Orig_Image_Blue.nPixelArr[nIndexCurSize])) / 100.0);

			if (sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize] > nIntensityStatMax)
			{
				sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize] = nIntensityStatMax;

			} //if (sOne_Orig_Image_Red.nPixelArr[nIndexMaxSize] > nIntensityStatMin)

			///////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (sOne_Orig_Image_Red.nPixelArr[nIndexCurSize] > nIntensityStatMin)
			{
				nNumOfPixelsAboveIntensityThreshold += 1;
				//fprintf(fout, "\n\n A new nNumOfPixelsAboveIntensityThreshold = %d, iWid = %d, iLen = %d, sOne_Orig_Image_WeightedGray.nPixelArr[%d] = %d", 
				//nNumOfPixelsAboveIntensityThreshold,iWid, iLen, nIndexMaxSize, sOne_Orig_Image_WeightedGray.nPixelArr[nIndexMaxSize]);

			} //if (sOne_Orig_Image_Red.nPixelArr[nIndexMaxSize] > nIntensityStatMin)

#ifdef PRINTING_SUBIMAGE
			if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
			{
				fprintf(fout, "\n\n An initial subimage iWid = %d, iLen = %d, sOne_Orig_Image_WeightedGray.nPixelArr[%d] = %d", 
					iWid, iLen, nIndexCurSize, sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize]);

				fprintf(fout, "\n Red = %d, Green = %d, Blue = %d, Gray = %d", sOne_Orig_Image_Red.nPixelArr[nIndexCurSize],
					sOne_Orig_Image_Green.nPixelArr[nIndexCurSize], sOne_Orig_Image_Blue.nPixelArr[nIndexCurSize], sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize]);

			} // if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
#endif // #ifdef PRINTING_SUBIMAGE
		
		} // for (iLen = 0; iLen < nImageLength; iLen++) //i

	}//for (iWid = 0; iWid < nImageWidth; iWid++) //j

	 ///////////////////////////////////////////////////////////////////////////

	  //  printf("\n\n After reading the image: please press any key to exit"); fflush(fout);  getchar(); exit(1);
	//printf("\n\n After reading the image: please press any key to continue"); fflush(fout);  getchar();


	///////////////////////////////////
	nRes = Histogram_Statistics_For_Orig_Image(
		&sOne_Orig_Image_WeightedGray, // ONE_ORIG_IMAGE *sOne_Orig_Imagef, //[nImageSizeMax]

		nObjectArea, //int &nObjectAreaf,

		fPercentageOfobjectArea, //float &fPercentageOfobjectAreaf,
		nHistogramArr, //int nHistogramArrf[], //[nNumOfHistogramBinsStat]
		fPercentagesInHistogramArr); // float fPercentagesInHistogramArrf[]);//[nNumOfHistogramBinsStat]

	printf("\n\n Gray conversion: nObjectArea = %d, fPercentageOfobjectArea = %E \n", nObjectArea, fPercentageOfobjectArea);
	fprintf(fout, "\n\n Gray conversion: nObjectArea = %d, fPercentageOfobjectArea = %E \n", nObjectArea, fPercentageOfobjectArea);

	for (iHistogramBin = 0; iHistogramBin < nNumOfHistogramBinsStat; iHistogramBin++)
	{
		printf("\n Gray: nHistogramArr[%d] = %d, fPercentagesInHistogramArr[%d] = %E",
			iHistogramBin, nHistogramArr[iHistogramBin], iHistogramBin, fPercentagesInHistogramArr[iHistogramBin]);

		fprintf(fout, "\n Gray: nHistogramArr[%d] = %d, fPercentagesInHistogramArr[%d] = %E",
			iHistogramBin, nHistogramArr[iHistogramBin], iHistogramBin, fPercentagesInHistogramArr[iHistogramBin]);
	} //for (iHistogramBin = 0; iHistogramBin < nNumOfHistogramBinsStat; iHistogramBin++)

	  //printf("\n\n After 'Histogram_Statistics_For_Orig_Image()': please press any key to exit"); fflush(fout); getchar(); exit(1);
	//printf("\n\n After 'Histogram_Statistics_For_Orig_Image()' Gray conversion: please press any key"); fflush(fout); getchar(); //exit(1);

																																 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	ONE_ORIG_IMAGE sOne_Filtered_Image;

	nRes = Initializing_One_Orig_Image_To_CurSize(
		nImageWidth, //const int nImageWidthf,
		nImageLength, //const int nImageLengthf,

		&sOne_Filtered_Image); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]


    if (INCLUDE_HSI){
	nRes = Initializing_One_Orig_Image_To_CurSize(
		nImageWidth, //const int nImageWidthf,
		nImageLength, //const int nImageLengthf,

		&sOne_Orig_Image_Saturation); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]

	nRes = Initializing_One_Orig_Image_To_CurSize(
		nImageWidth, //const int nImageWidthf,
		nImageLength, //const int nImageLengthf,

		&sOne_Orig_Image_Intensity); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]

	nRes = Initializing_One_Orig_Image_To_CurSize(
		nImageWidth, //const int nImageWidthf,
		nImageLength, //const int nImageLengthf,

		&sOne_Orig_Image_Hue); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]

	nRes = HSI_Colors(
		&sOne_Orig_Image_Red, //const ONE_ORIG_IMAGE *sOne_Orig_Image_Redf,
		&sOne_Orig_Image_Green, //const ONE_ORIG_IMAGE *sOne_Orig_Image_Greenf,
		&sOne_Orig_Image_Blue, //const ONE_ORIG_IMAGE *sOne_Orig_Image_Bluef,

		&sOne_Orig_Image_Intensity, //ONE_ORIG_IMAGE *sOne_Orig_Image_Intensityf,
		&sOne_Orig_Image_Saturation, //ONE_ORIG_IMAGE *sOne_Orig_Image_Saturationf,
		&sOne_Orig_Image_Hue); // ONE_ORIG_IMAGE *sOne_Orig_Image_Huef);

	//printf("\n\n After 'HSI_Colors()': please press any key"); fflush(fout); getchar(); //exit(1);

        if (CHOOSE_SATURATION){

	nRes = Sobel_Filtering(

		&sOne_Orig_Image_Saturation, //const ONE_ORIG_IMAGE *sOne_Orig_Imagef,
		&sOne_Filtered_Image); // ONE_ORIG_IMAGE *sOne_Filtered_Imagef)

        } // #ifdef CHOOSE_SATURATION
							   //////////////////////////////////////////////////////////////////////////

        if (CHOOSE_HUE){

	nRes = Sobel_Filtering(

		&sOne_Orig_Image_Hue, //const ONE_ORIG_IMAGE *sOne_Orig_Imagef,
		&sOne_Filtered_Image); // ONE_ORIG_IMAGE *sOne_Filtered_Imagef)

        } // #ifdef CHOOSE_HUE
///////////////////////////////////////////////////////////////////////////////////////
	
        if (CHOOSE_INTENSITY){

	nRes = Sobel_Filtering(

		&sOne_Orig_Image_Intensity, //const ONE_ORIG_IMAGE *sOne_Orig_Imagef,
		&sOne_Filtered_Image); // ONE_ORIG_IMAGE *sOne_Filtered_Image)

        } // #ifdef CHOOSE_INTENSITY

    } // #ifdef INCLUDE_HSI

    if (INCLUDE_WEIGHTED_GRAY){
	nRes = Sobel_Filtering(
		&sOne_Orig_Image_WeightedGray, //const ONE_ORIG_IMAGE *sOne_Orig_Imagef,
		&sOne_Filtered_Image); // ONE_ORIG_IMAGE *sOne_Filtered_Image)

    } // INCLUDE_WEIGHTED_GRAY

		/*
		// use 'nIntensityThreshold' to change the number of object pixels!
		for (iWid = 0; iWid < nWidth; iWid++)
		{
			for (iLen = 0; iLen < nLength; iLen++)
			{
			nIndexCurSize = iLen + (iWidn*ImageLength);


			sOne_Orig_Image.nPixelArr[nIndexCurSize] = nTest_ImageArr[nIndexCurSize];

			//printf( "%d %d, ", nIndexCurSize, nTest_ImageArr[nIndexCurSize]);
			//	fprintf(fout, "\n iWid = %d, iLen = %d, nTest_ImageArr[%d] = %d ", iWid, iLen,nIndexCurSize, nTest_ImageArr[nIndexCurSize]);


			if (nTest_ImageArr[nIndexCurSize] > nIntensityStatMin)
			nNumOfPixelsAboveIntensityThreshold += 1;

			if ((iLen / 20) * 20 == iLen)
			{
			//printf("\n iLen = %d, iWid = %d\n", iLen, iWid);
			//fprintf(fout, "\n iLen = %d, iWid = %d\n", iLen, iWid);
			//fflush(fout);
			} //if ((iLen / 20)*20 == iLen)

			} //for (iLen = 0; iLen < nLength; iLen++)
		} //for (iWid = 0; iWid < nWidth; iWid++)
		*/
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//printf("\n\n Before drawing pixels of edges: nImageWidth = %d, nImageLength = %d, please press any key", nImageWidth, nImageLength); getchar();

	int bytesOfWidth = testImg.pitchInBytes();

	int nStep = testImg.pitchInBytes() / (testImg.width() * sizeof(unsigned char));

	// Save to file
	Image imageToSave(nImageLength, nImageWidth, bytesOfWidth);

	//initial shading of the image 
	for (iWid = 0; iWid < nImageWidth; iWid++) //j
	{
#ifdef PRINTING_SUBIMAGE

		if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax)
		{
			fprintf(fout, "\n\n Filtered: iWid = %d\n", iWid);
		} // if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax)

#endif // #ifdef PRINTING_SUBIMAGE

		for (iLen = 0; iLen < nImageLength; iLen++) //i
		{
			nIndexCurSize = iLen + (iWid*nImageLength);

		//	fprintf(fout, "\n Filtered: iWid = %d, iLen = %d, sOne_Filtered_Image.nPixelArr[%d] = %d", iWid, iLen, nIndexCurSize, sOne_Filtered_Image.nPixelArr[nIndexCurSize]);

#ifdef PRINTING_SUBIMAGE
			if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
			{
				fprintf(fout, "\n iWid = %d, iLen = %d, sOne_Filtered_Image.nPixelArr[%d] = %d, sOne_Orig_Image_WeightedGray.nPixelArr[%d] = %d, sOne_Orig_Image_Red.nPixelArr[%d] = %d, sOne_Orig_Image_Green.nPixelArr[%d] = %d,  sOne_Orig_Image_Blue.nPixelArr[%d] = %d",
					iWid, iLen, nIndexCurSize, sOne_Filtered_Image.nPixelArr[nIndexCurSize], nIndexCurSize, sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize],
					nIndexCurSize, sOne_Orig_Image_Red.nPixelArr[nIndexCurSize], nIndexCurSize, sOne_Orig_Image_Green.nPixelArr[nIndexCurSize], nIndexCurSize, sOne_Orig_Image_Blue.nPixelArr[nIndexCurSize]);

			} // if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
#endif // #ifdef PRINTING_SUBIMAGE

            if (USING_ONLY_BLACK_AND_WHITE_COLORS){
			if (sOne_Filtered_Image.nPixelArr[nIndexCurSize] < nIntensityStatMin)
			{
				//GrayByColorArr[0] = 0; // 1 * nGrayLevelIntensityForDenseArea; //black color for contours
				//GrayByColorArr[1] = 0; // 1 * nGrayLevelIntensityForDenseArea;
				//GrayByColorArr[2] = 0; // 1 * nGrayLevelIntensityForDenseArea;
				imageToSave(iWid, iLen, R) = 0;
				imageToSave(iWid, iLen, G) = 0;
				imageToSave(iWid, iLen, B) = 0;

			} // if (sOne_Orig_Image.nPixelArr[nIndexCurSize] < nIntensityStatMin)
			else if (sOne_Filtered_Image.nPixelArr[nIndexCurSize] >= nIntensityStatMin)
			{
			//	GrayByColorArr[0] = nIntensityStatMax; // 1 * nGrayLevelIntensityToDrawContours; //white color for the remainder
				//GrayByColorArr[1] = nIntensityStatMax; // 1 * nGrayLevelIntensityToDrawContours;
				//GrayByColorArr[2] = nIntensityStatMax; // 1 * nGrayLevelIntensityToDrawContours;

				imageToSave(iWid, iLen, R) = nIntensityStatMax;
				imageToSave(iWid, iLen, G) = nIntensityStatMax;
				imageToSave(iWid, iLen, B) = nIntensityStatMax;
			} // else if (sOne_Orig_Image.nPixelArr[nIndexCurSize] >= nIntensityStatMin)
            } // #ifdef USING_ONLY_BLACK_AND_WHITE_COLORS


            if (!USING_ONLY_BLACK_AND_WHITE_COLORS){

			//GrayByColorArr[0] = sOne_Filtered_Image.nPixelArr[nIndexCurSize];
			//GrayByColorArr[1] = sOne_Filtered_Image.nPixelArr[nIndexCurSize];
			//GrayByColorArr[2] = sOne_Filtered_Image.nPixelArr[nIndexCurSize];

			//imageToSave(j, i, R) = imageToSave(j, i, G) = imageToSave(j, i, B) = sGrayscale_Image.nGrayscale_Arr[nIndexOfPixelMax];
			imageToSave(iWid, iLen, R) = sOne_Filtered_Image.nPixelArr[nIndexCurSize];
			imageToSave(iWid, iLen, G) = sOne_Filtered_Image.nPixelArr[nIndexCurSize];
			imageToSave(iWid, iLen, B) = sOne_Filtered_Image.nPixelArr[nIndexCurSize];

            } //#ifndef USING_ONLY_BLACK_AND_WHITE_COLORS

			imageToSave(iWid, iLen, A) = testImg(iWid, iLen, A);
		} //for (iLen = 0; iLen < nLength; iLen++)

	} //for (iWid = 0; iWid < nImageWidth; iWid++)

    string saveToFile;
    size_t pos = files[iFile].find_last_of("/");
    string imageName = files[iFile].substr(pos+1);
    saveToFile = outDirectory + "/" + imageName;
	bool success = imageToSave.write(saveToFile);
    if (!success){
        printf("\n\n Errors in saving grayscale image!");
        exit(-1);
    }
	printf("\n\n The grayscale image has been saved");

	//
    //printf("\n\n After drawing the image: please press any key to exit"); fflush(fout);  getchar();  exit(1);

/////////////////////////////////////////////////////////////////////////////////
	delete[] sOne_Orig_Image_WeightedGray.nPixelArr;

	delete[] sOne_Orig_Image_Red.nPixelArr;
	delete[] sOne_Orig_Image_Green.nPixelArr;
	delete[] sOne_Orig_Image_Blue.nPixelArr;

	delete[] sOne_Filtered_Image.nPixelArr;

    if (INCLUDE_HSI){

	delete[] sOne_Orig_Image_Saturation.nPixelArr;
	delete[] sOne_Orig_Image_Intensity.nPixelArr;
	delete[] sOne_Orig_Image_Hue.nPixelArr;
    } //#ifdef INCLUDE_HSI
  /////////////////////////////////////////////////////////////////////////////////
}
	fclose(fout);
	return 1;
} //int main()

  /////////////////////////////////////////////////////////////////////////////////////////////////////
int Histogram_Statistics_For_Orig_Image(

	ONE_ORIG_IMAGE *sOne_Orig_Imagef, //[nImageSizeMax]

	int &nObjectAreaf,
	float &fPercentageOfobjectAreaf,
	int nHistogramArrf[], //[nNumOfHistogramBinsStat]
	float fPercentagesInHistogramArrf[]) //[nNumOfHistogramBinsStat]
{
	int
		nImageWidthf = sOne_Orig_Imagef->nWidCur,
		nImageLengthf = sOne_Orig_Imagef->nLenCur,

		nAreaOfTheWholeImagef,
		nWidthOfABinInHistogramf = (nIntensityStatMax - nIntensityStatMin) / nNumOfHistogramBinsStat,

		nNumOfHistogramBinCurf,

		nIntensityCurf,
		nIndexCurSizef,

		iHistogramBinf,
		iLenf,
		iWidf;

	if (nImageLengthf < 1 || nImageLengthf > nLenMax || nImageWidthf < 1 || nImageWidthf > nWidMax)
	{
		printf("\n\n An error in 'Histogram_Statistics_For_Orig_Image': nImageLengthf = %d, nLenMax = %d, nImageWidthf = %d, nWidMax = %d",
			nImageLengthf, nLenMax, nImageWidthf, nWidMax);

		fprintf(fout, "\n\n An error in 'Histogram_Statistics_For_Orig_Image': nImageLengthf = %d, nLenMax = %d, nImageWidthf = %d, nWidMax = %d",
			nImageLengthf, nLenMax, nImageWidthf, nWidMax);

		//printf("\n\n Please press any key to exit:");
		//fflush(fout); getchar();
        exit(1);
	} // if (nImageLengthf < 1 || nImageLengthf > nLenMax || nImageWidthf < 1 || nImageWidthf > nWidMax)

	  /////////////////////////////////////////////////

	nAreaOfTheWholeImagef = nImageWidthf*nImageLengthf;

	if (nAreaOfTheWholeImagef <= 0)
	{
		printf("\n\n An error in 'Histogram_Statistics_For_Orig_Image': nAreaOfTheWholeImagef = %d <= 0", nAreaOfTheWholeImagef);

		fprintf(fout, "\n\n An error in 'Histogram_Statistics_For_Orig_Image': nAreaOfTheWholeImagef = %d <= 0", nAreaOfTheWholeImagef);
		//printf("\n\n Please press any key to exit:");
		//fflush(fout); getchar();
        exit(1);
	} // if (nAreaOfTheWholeImagef <= 0)

	nObjectAreaf = 0;

	//memset(nHistogramArrf, 0, sizeof(nHistogramArrf));
	//memset(fPercentagesInHistogramArrf, 0.0, sizeof(fPercentagesInHistogramArrf));

	for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)
	{
		nHistogramArrf[iHistogramBinf] = 0;
		fPercentagesInHistogramArrf[iHistogramBinf] = 0.0;

	} //for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)

	  //////////////////////////////////////////////////////
	  // 
	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//fprintf(fout, "\n\n iWidf = %d: ", iWidf);

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexCurSizef = iLenf + (iWidf*nImageLengthf);

			nIntensityCurf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizef];

			//fprintf(fout, " %d:%d %d,", iWidf, iLenf, nIntensityCurf);

			if (nIntensityCurf >= nIntensityThresholdForDense)
			{
				sOne_Orig_Imagef->nAreaDense += 1;

			} //if (nIntensityCurf >= nIntensityThresholdForDense)

			if (nIntensityCurf >= nIntensityStatMin)
			{
				nObjectAreaf += 1;

				nNumOfHistogramBinCurf = (nIntensityCurf - nIntensityStatMin) / nWidthOfABinInHistogramf;

				if (nNumOfHistogramBinCurf >= nNumOfHistogramBinsStat)
					nNumOfHistogramBinCurf = nNumOfHistogramBinsStat - 1;

				nHistogramArrf[nNumOfHistogramBinCurf] += 1;
			} //if (nIntensityCurf >= nIntensityStatMin)

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)

	} //for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	fPercentageOfobjectAreaf = (float)(nObjectAreaf) / (float)(nAreaOfTheWholeImagef);

	for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)
	{
		if (nObjectAreaf > 0)
		{
			fPercentagesInHistogramArrf[iHistogramBinf] = (float)(nHistogramArrf[iHistogramBinf]) / (float)(nObjectAreaf);
		} //if (nObjectAreaf > 0)
		else if (nObjectAreaf == 0)
		{
			printf("\n\n An error in 'Histogram_Statistics_For_Orig_Image': nObjectAreaf == 0");
			printf("\n Please try decreasing 'nIntensityStatMin'");

			fprintf(fout, "\n\n An error in 'Histogram_Statistics_For_Orig_Image': nObjectAreaf == 0");
			fprintf(fout, "\n Please try decreasing 'nIntensityStatMin'");

			//printf("\n\n Please press any key to exit:");
			//fflush(fout); getchar();
            exit(1);
		} // else if (nObjectAreaf == 0)

	} //for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)

	sOne_Orig_Imagef->nAreaForStat = nObjectAreaf;

	sOne_Orig_Imagef->fPercentageOfobjectArea = fPercentageOfobjectAreaf;

	if (sOne_Orig_Imagef->nAreaForStat < sOne_Orig_Imagef->nAreaDense)
	{
		printf("\n\n An error in 'Histogram_Statistics_For_Orig_Image': sOne_Orig_Imagef->nAreaForStat = %d < sOne_Orig_Imagef->nAreaDense = %d",
			sOne_Orig_Imagef->nAreaForStat, sOne_Orig_Imagef->nAreaDense);

		printf("\n Area of the object is less than the dense area");

		fprintf(fout, "\n\n An error in 'Histogram_Statistics_For_Orig_Image': sOne_Orig_Imagef->nAreaForStat = %d < sOne_Orig_Imagef->nAreaDense = %d",
			sOne_Orig_Imagef->nAreaForStat, sOne_Orig_Imagef->nAreaDense);

		fprintf(fout, "\n Area of the object is less than the dense area");

		//printf("\n\n Please press any key to exit:");
		//fflush(fout); getchar();
        exit(1);

	} // if (sOne_Orig_Imagef->nAreaForStat < sOne_Orig_Imagef->nAreaDense)

	return 1;
} //Histogram_Statistics_For_Orig_Image(...

  //////////////////////////////////////////////////////////////////////

int Initializing_One_Orig_Image_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	ONE_ORIG_IMAGE *sOne_Orig_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf*nImageLengthf,
		iWidf,
		iLenf;

	sOne_Orig_Imagef->nWidCur = nImageWidthf;
	sOne_Orig_Imagef->nLenCur = nImageLengthf;

    sOne_Orig_Imagef->nAreaDense = 0;
    sOne_Orig_Imagef->nAreaForStat = 0;
    
	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];
	sOne_Orig_Imagef->nPixelArr = new int[nImageSizeCurf];

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sOne_Orig_Imagef->nPixelArr[nIndexOfPixelCurf] = -1;

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nWidMax; iWidf++)

	return 1;
} //int Initializing_One_Orig_Image_To_CurSize(...

  //////////////////////////////////////////////////////////////////////////////////////
int Sobel_Filtering(
	const ONE_ORIG_IMAGE *sOne_Orig_Imagef,
	ONE_ORIG_IMAGE *sOne_Filtered_Imagef)
{
/*
	int Initializing_One_Orig_Image_To_Max(

		ONE_ORIG_IMAGE *sOne_Orig_Imagef); //[nImageSizeMax]
*/
	int Initializing_One_Orig_Image_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]

	int

		nIndexCurSizef,
		nIndexCurSizeOfANeighborf,

		nSumLenf,
		nSumWidf,

		nLenNeighf,
		nWidNeighf,

		nImageWidthf = sOne_Orig_Imagef->nWidCur,
		nImageLengthf = sOne_Orig_Imagef->nLenCur,

		nIntensityNeighf,

		nSumOfABS_Of_nSumLenf_and_nSumWidf = 0,
		nResf,
		iWidf,
		iLenf;

	//nResf = Initializing_One_Orig_Image_To_Max(
		//sOne_Filtered_Imagef); // ONE_ORIG_IMAGE *sOne_Orig_Imagef) //[nImageSizeMax]

//	nResf = Initializing_One_Orig_Image_To_CurSize(
	//	nImageWidthf, //const int nImageWidthf,
		//nImageLengthf, //const int nImageLengthf,

		//sOne_Filtered_Imagef); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);
	
	//3x3 Sobel filter
    if( SOBEL_FILTER_SIZE_3){
        // use 'nIntensityThreshold' to change the number of object pixels!
        for (iWidf = 0; iWidf < sOne_Filtered_Imagef->nWidCur; iWidf++)
        {
            fprintf(fout, "\n\n 'Sobel_Filtering': iWidf = %d", iWidf);
            
            for (iLenf = 0; iLenf < sOne_Filtered_Imagef->nLenCur; iLenf++)
            {
                nIndexCurSizef = iLenf + (iWidf*nImageLengthf);
                
                if (iWidf == 0 || iWidf == sOne_Filtered_Imagef->nWidCur - 1)
                {
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax;
                    continue;
                } // if (iWidf == 0 || iWidf == sOne_Filtered_Imagef->nWidCur - 1)
                
                if (iLenf == 0 || iLenf == sOne_Filtered_Imagef->nLenCur - 1)
                {
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax;
                    continue;
                } // if (iLenf == 0 || iLenf == sOne_Filtered_Imagef->nLenCur - 1)
                
                nSumLenf = 0;
                nSumWidf = 0;
                
                nSumOfABS_Of_nSumLenf_and_nSumWidf = 0;
                /////////////////////////////////////////////////////////////////////
                //1 -- addition for nSumLenf
                nLenNeighf = iLenf - 1;
                nWidNeighf = iWidf + 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if( SOBEL_VERSION2_3){
                    nSumLenf += nIntensityNeighf;
                    
                    nSumWidf -= nIntensityNeighf;
                } // if (!VERSION_2_OF_SOBEL_3x3){
                
                if( SOBEL_VERSION2_3){
                    nSumLenf += 3 * nIntensityNeighf;
                    
                    nSumWidf -= 3 * nIntensityNeighf;
                } // if (VERSION_2_OF_SOBEL_3x3){
                
                //fprintf(fout, "\n\n 1: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);
                
                //fprintf(fout, "\n nWidNeighf = %d, nLenNeighf = %d, sOne_Orig_Imagef->nPixelArr[%d] = %d",
                //nWidNeighf, nLenNeighf, nIndexCurSizeOfANeighborf,sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf]);
                
                /////////////////////////////////////////////////////////////////////
                //2 -- addition for nSumLenf
                nLenNeighf = iLenf;
                nWidNeighf = iWidf + 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if(!SOBEL_VERSION2_3){
                    nSumLenf += 2 * nIntensityNeighf;
                } // if (!VERSION_2_OF_SOBEL_3x3){
                
                
                if (SOBEL_VERSION2_3){
                    nSumLenf += 10 * nIntensityNeighf;
                } // if (VERSION_2_OF_SOBEL_3x3){
                
                //fprintf(fout, "\n 2: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);
                
                /////////////////////////////////////////////////////////////////////
                //3 -- addition for nSumLenf
                nLenNeighf = iLenf + 1;
                nWidNeighf = iWidf + 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (!SOBEL_VERSION2_3){
                    nSumLenf += nIntensityNeighf;
                    
                    nSumWidf += nIntensityNeighf;
                } // if (!VERSION_2_OF_SOBEL_3x3){
                
                
                if (SOBEL_VERSION2_3){
                    nSumLenf += 3 * nIntensityNeighf;
                    
                    nSumWidf += 3 * nIntensityNeighf;
                } // if (VERSION_2_OF_SOBEL_3x3){
                
                //fprintf(fout, "\n 3: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);
                
                /////////////////////////////////////////////////////////////////////
                //4 -- subtraction for nSumLenf
                
                nLenNeighf = iLenf - 1;
                nWidNeighf = iWidf - 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (!SOBEL_VERSION2_3){
                    nSumLenf -= nIntensityNeighf;
                    nSumWidf -= nIntensityNeighf;
                } // if (!VERSION_2_OF_SOBEL_3x3){
                
                if (SOBEL_VERSION2_3){
                    nSumLenf -= 3 * nIntensityNeighf;
                    
                    nSumWidf -= 3 * nIntensityNeighf;
                } // if (VERSION_2_OF_SOBEL_3x3){
                
                //fprintf(fout, "\n 4: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);
                
                /////////////////////////////////////////////////////////////////////
                //5 -- subtraction for nSumLenf
                nLenNeighf = iLenf;
                nWidNeighf = iWidf - 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (!SOBEL_VERSION2_3){
                    nSumLenf -= 2 * nIntensityNeighf;
                } // if (!VERSION_2_OF_SOBEL_3x3){
                
                if (SOBEL_VERSION2_3){
                    nSumLenf -= 10 * nIntensityNeighf;
                } // if (VERSION_2_OF_SOBEL_3x3){
                
                //fprintf(fout, "\n 5: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);
                
                /////////////////////////////////////////////////////////////////////
                //6 -- subtraction for nSumLenf
                nLenNeighf = iLenf + 1;
                nWidNeighf = iWidf - 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (!SOBEL_VERSION2_3){
                    nSumLenf -= nIntensityNeighf;
                    
                    nSumWidf += nIntensityNeighf;
                } // if (!VERSION_2_OF_SOBEL_3x3){
                
                if (SOBEL_VERSION2_3){
                    nSumLenf -= 3 * nIntensityNeighf;
                    
                    nSumWidf += 3 * nIntensityNeighf;
                } // if (VERSION_2_OF_SOBEL_3x3){
                
                //fprintf(fout, "\n 6: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);
                
                /////////////////////////////////////////////////////////////////////
                //7 -- addition  for nSumWidf
                nLenNeighf = iLenf + 1;
                nWidNeighf = iWidf;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (!SOBEL_VERSION2_3){
                    //addition  for nSumWidf
                    nSumWidf += 2 * nIntensityNeighf;
                } // if (!VERSION_2_OF_SOBEL_3x3){
                
                if (SOBEL_VERSION2_3){
                    //addition  for nSumWidf
                    nSumWidf += 10 * nIntensityNeighf;
                } // if (VERSION_2_OF_SOBEL_3x3){
                
                //    fprintf(fout, "\n 7: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);
                
                /////////////////////////////////////////////////////////////////////
                //8 -- subtraction for nSumWidf
                nLenNeighf = iLenf - 1;
                nWidNeighf = iWidf;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (!SOBEL_VERSION2_3){
                    nSumWidf -= 2 * nIntensityNeighf;
                } // if (!VERSION_2_OF_SOBEL_3x3){
                
                if (SOBEL_VERSION2_3){
                    nSumWidf -= 10 * nIntensityNeighf;
                } // if (VERSION_2_OF_SOBEL_3x3){
                
                if (SOBEL_VERSION2_3){
                    
                    //fprintf(fout, "\n 8: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);
                    
                    ///////////////////////////////////////////////////////////////////////////////////
                    nSumLenf = (int)((float)(nSumLenf) / fWeight_For_2nd_Version);
                    nSumWidf = (int)((float)(nSumWidf) / fWeight_For_2nd_Version);
                    
                } // if (VERSION_2_OF_SOBEL_3x3){
                /////////////////////////////////////////////////////////////////////
                
                
                if (USE_SUMS_OF_IMAGE_GRADIENTS){
                    
                    //The sum of absolute values
                    if (nSumLenf < 0)
                    {
                        nSumOfABS_Of_nSumLenf_and_nSumWidf += (-nSumLenf);
                    } //if (nSumLenf < 0)
                    else if (nSumLenf > 0)
                    {
                        
                        nSumOfABS_Of_nSumLenf_and_nSumWidf += nSumLenf;
                    } //else if (nSumLenf > 0)
                    
                    if (nSumWidf < 0)
                    {
                        nSumOfABS_Of_nSumLenf_and_nSumWidf += (-nSumWidf);
                    } //if (nSumWidf < 0)
                    else if (nSumWidf > 0)
                    {
                        
                        nSumOfABS_Of_nSumLenf_and_nSumWidf += nSumWidf;
                    } //else if (nSumWidf > 0)
                    
                    if (USING_ONLY_BLACK_AND_WHITE_COLORS){
                        if (nSumOfABS_Of_nSumLenf_and_nSumWidf > nIntensityThresholdForSobelMax)
                            nSumOfABS_Of_nSumLenf_and_nSumWidf = nIntensityStatMax;
                        
                        if (nSumOfABS_Of_nSumLenf_and_nSumWidf < nIntensityThresholdForSobelMin)
                            nSumOfABS_Of_nSumLenf_and_nSumWidf = 0;
                    } //#ifdef USING_ONLY_BLACK_AND_WHITE_COLORS
                    
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nSumOfABS_Of_nSumLenf_and_nSumWidf;
                    
                } // #ifdef USE_SUMS_OF_IMAGE_GRADIENTS
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
                if (!USE_SUMS_OF_IMAGE_GRADIENTS){
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = (int)(sqrt((float)(nSumLenf*nSumLenf + nSumWidf*nSumWidf) / fNormalizing));
                    //sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = (int)(sqrt( (float)(nSumLenf*nSumLenf + nSumWidf*nSumWidf) ));
                    
                    
                    if (USING_ONLY_BLACK_AND_WHITE_COLORS){
                        if (sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] >= nIntensityThresholdForSobelMax)
                            sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = 0;
                        else
                            sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = 255;
                    } //#ifdef USING_ONLY_BLACK_AND_WHITE_COLORS
                    
                } // #ifndef USE_SUMS_OF_IMAGE_GRADIENTS
                
                // The histogram threshold could be applied here
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] > nIntensityStatMax)
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax;
                else if (sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] < 0)
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = 0;
                
                if (!BLACK_BACKGROUND_WHITE_CONTOURS){
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax - sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef];
                } //#ifndef BLACK_BACKGROUND_WHITE_CONTOURS
            
            //fprintf(fout, "\n iWidf = %d, iLenf = %d, sOne_Filtered_Imagef->nPixelArr[%d] = %d", iWidf, iLenf, nIndexCurSizef, sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef]);
            
            //printf("\n\n Please press any key to exit"); fflush(fout); getchar(); exit(1);
        } //for (iLenf = 0; iLenf < sOne_Filtered_Imagef->nLenCur; iLenf++)
        
    } //for (iWidf = 0; iWidf < sOne_Filtered_Imagef->nWidCur; iWidf++)

    } // #ifdef DIM3x3_SOBEL_FILTER

	  //////////////////////////////////////////////////////////////////////////////////////

    if (SOBEL_FILTER_SIZE_5){

        // use 'nIntensityThreshold' to change the number of object pixels!
        for (iWidf = 0; iWidf < sOne_Filtered_Imagef->nWidCur; iWidf++)
        {
            for (iLenf = 0; iLenf < sOne_Filtered_Imagef->nLenCur; iLenf++)
            {
                nIndexCurSizef = iLenf + (iWidf*nImageLengthf);
                
                if (iWidf == 0 || iWidf == sOne_Filtered_Imagef->nWidCur - 1 || iWidf == sOne_Filtered_Imagef->nWidCur - 2)
                {
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax;
                    continue;
                } // if (iWidf == 0 || iWidf == sOne_Filtered_Imagef->nWidCur - 1 || iWidf == sOne_Filtered_Imagef->nWidCur - 2)
                
                if (iLenf == 0 || iLenf == sOne_Filtered_Imagef->nLenCur - 1 || iLenf == sOne_Filtered_Imagef->nLenCur - 2)
                {
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax;
                    continue;
                } // if (iLenf == 0 || iLenf == sOne_Filtered_Imagef->nLenCur - 1 || iLenf == sOne_Filtered_Imagef->nLenCur - 2)
                
                nSumLenf = 0;
                nSumWidf = 0;
                
                nSumOfABS_Of_nSumLenf_and_nSumWidf = 0;
                
                //////////////////////////////////////////////////////////////////////////////////
                
                //1 -- addition for nSumLenf
                nLenNeighf = iLenf - 2;
                nWidNeighf = iWidf - 2;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += nIntensityNeighf;
                    
                    nSumWidf += nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 3 * nIntensityNeighf;
                    
                    nSumWidf -= 3 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //2 -- addition for nSumLenf
                nLenNeighf = iLenf - 1;
                nWidNeighf = iWidf - 2;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 4 * nIntensityNeighf;
                    
                    nSumWidf += 2 * nIntensityNeighf;
                    
                } // if (SOBEL_VERSION2_5){
                
                
                if (SOBEL_VERSION2_5){
                    
                    nSumLenf += 10 * nIntensityNeighf; //
                    nSumWidf -= 3 * nIntensityNeighf;
                    
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //3 -- addition for nSumLenf
                nLenNeighf = iLenf;
                nWidNeighf = iWidf - 2;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 6 * nIntensityNeighf;
                    
                    nSumWidf += 0 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 0 * nIntensityNeighf; //
                    
                    nSumWidf -= 3 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //4 -- addition for nSumLenf
                nLenNeighf = iLenf + 1;
                nWidNeighf = iWidf - 2;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 4 * nIntensityNeighf;
                    
                    nSumWidf -= 2 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 10 * nIntensityNeighf; //
                    
                    nSumWidf -= 3 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //5 -- addition for nSumLenf
                nLenNeighf = iLenf + 2;
                nWidNeighf = iWidf - 2;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 1 * nIntensityNeighf;
                    
                    nSumWidf -= 1 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 3 * nIntensityNeighf; //
                    
                    nSumWidf -= 3 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                //2nd row
                /////////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////
                //6 -- addition for nSumLenf
                nLenNeighf = iLenf - 2;
                nWidNeighf = iWidf - 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 2 * nIntensityNeighf;
                    
                    nSumWidf += 4 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 3 * nIntensityNeighf;
                    
                    nSumWidf -= 10 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //7 -- addition for nSumLenf
                nLenNeighf = iLenf - 1;
                nWidNeighf = iWidf - 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 8 * nIntensityNeighf;
                    
                    nSumWidf += 8 * nIntensityNeighf;
                    
                } // if (SOBEL_VERSION2_5){
                
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 10 * nIntensityNeighf;
                    
                    nSumWidf -= 10 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //8 -- addition for nSumLenf
                nLenNeighf = iLenf;
                nWidNeighf = iWidf - 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 12 * nIntensityNeighf;
                    
                    nSumWidf += 0 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 0 * nIntensityNeighf;
                    
                    nSumWidf -= 10 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //9 -- addition for nSumLenf
                nLenNeighf = iLenf + 1;
                nWidNeighf = iWidf - 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 8 * nIntensityNeighf;
                    
                    nSumWidf -= 8 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 10 * nIntensityNeighf;
                    
                    nSumWidf -= 10 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //10 -- addition for nSumLenf
                nLenNeighf = iLenf + 2;
                nWidNeighf = iWidf - 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 2 * nIntensityNeighf;
                    
                    nSumWidf -= 4 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 3 * nIntensityNeighf;
                    
                    nSumWidf -= 10 * nIntensityNeighf;
                    
                } // if (SOBEL_VERSION2_5){
                
                //3rd row
                /////////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////
                //11 -- addition for nSumLenf
                nLenNeighf = iLenf - 2;
                nWidNeighf = iWidf;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 0 * nIntensityNeighf;
                    
                    nSumWidf += 6 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 3 * nIntensityNeighf;
                    
                    nSumWidf -= 0 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                
                /////////////////////////////////////////////////////////////////////
                //12 -- addition for nSumLenf
                nLenNeighf = iLenf - 1;
                nWidNeighf = iWidf;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 0 * nIntensityNeighf;
                    
                    nSumWidf += 12 * nIntensityNeighf;
                    
                } // if (SOBEL_VERSION2_5){
                
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 10 * nIntensityNeighf;
                    
                    nSumWidf -= 0 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //13 -- addition for nSumLenf
                nLenNeighf = iLenf;
                nWidNeighf = iWidf;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 0 * nIntensityNeighf;
                    
                    nSumWidf += 0 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 0 * nIntensityNeighf;
                    
                    nSumWidf += 0 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //14 -- addition for nSumLenf
                nLenNeighf = iLenf + 1;
                nWidNeighf = iWidf;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 0 * nIntensityNeighf;
                    
                    nSumWidf -= 12 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 10 * nIntensityNeighf;
                    
                    nSumWidf += 0 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //15 -- addition for nSumLenf
                nLenNeighf = iLenf + 2;
                nWidNeighf = iWidf;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 0 * nIntensityNeighf;
                    
                    nSumWidf -= 6 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 3 * nIntensityNeighf;
                    
                    nSumWidf += 0 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                //4th row
                ////////////////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////////////////
                //16 -- addition for nSumLenf
                nLenNeighf = iLenf - 2;
                nWidNeighf = iWidf + 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 2 * nIntensityNeighf;
                    
                    nSumWidf += 4 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 3 * nIntensityNeighf;
                    
                    nSumWidf += 10 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //17 -- addition for nSumLenf
                nLenNeighf = iLenf - 1;
                nWidNeighf = iWidf + 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 8 * nIntensityNeighf;
                    
                    nSumWidf += 8 * nIntensityNeighf;
                    
                } // if (SOBEL_VERSION2_5){
                
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 10 * nIntensityNeighf;
                    
                    nSumWidf += 10 * nIntensityNeighf;
                    
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //18 -- addition for nSumLenf
                nLenNeighf = iLenf;
                nWidNeighf = iWidf + 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 12 * nIntensityNeighf;
                    
                    nSumWidf += 0 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 0 * nIntensityNeighf;
                    
                    nSumWidf += 10 * nIntensityNeighf;
                    
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //19 -- addition for nSumLenf
                nLenNeighf = iLenf + 1;
                nWidNeighf = iWidf + 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 8 * nIntensityNeighf;
                    
                    nSumWidf -= 8 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 10 * nIntensityNeighf;
                    
                    nSumWidf += 10 * nIntensityNeighf;
                    
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //20 -- addition for nSumLenf
                nLenNeighf = iLenf + 2;
                nWidNeighf = iWidf + 1;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 2 * nIntensityNeighf;
                    
                    nSumWidf -= 4 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 3 * nIntensityNeighf;
                    
                    nSumWidf += 10 * nIntensityNeighf;
                    
                } // if (SOBEL_VERSION2_5){
                
                //5th row
                ////////////////////////////////////////////////////////////////////
                //21 -- addition for nSumLenf
                nLenNeighf = iLenf - 2;
                nWidNeighf = iWidf + 2;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 1 * nIntensityNeighf;
                    
                    nSumWidf += 1 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 3 * nIntensityNeighf;
                    
                    nSumWidf += 3 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                
                /////////////////////////////////////////////////////////////////////
                //22 -- addition for nSumLenf
                nLenNeighf = iLenf - 1;
                nWidNeighf = iWidf + 2;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 4 * nIntensityNeighf;
                    
                    nSumWidf += 2 * nIntensityNeighf;
                    
                } // if (SOBEL_VERSION2_5){
                
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 10 * nIntensityNeighf;
                    
                    nSumWidf += 3 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //23 -- addition for nSumLenf
                nLenNeighf = iLenf;
                nWidNeighf = iWidf + 2;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 6 * nIntensityNeighf;
                    
                    nSumWidf += 0 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                
                if (SOBEL_VERSION2_5){
                    nSumLenf += 0 * nIntensityNeighf;
                    
                    nSumWidf += 3 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //24 -- addition for nSumLenf
                nLenNeighf = iLenf + 1;
                nWidNeighf = iWidf + 2;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 4 * nIntensityNeighf;
                    
                    nSumWidf -= 2 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 10 * nIntensityNeighf;
                    
                    nSumWidf += 3 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                /////////////////////////////////////////////////////////////////////
                //25 -- addition for nSumLenf
                nLenNeighf = iLenf + 2;
                nWidNeighf = iWidf + 2;
                
                nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
                nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 1 * nIntensityNeighf;
                    
                    nSumWidf -= 1 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                if (SOBEL_VERSION2_5){
                    nSumLenf -= 3 * nIntensityNeighf;
                    
                    nSumWidf += 3 * nIntensityNeighf;
                } // if (SOBEL_VERSION2_5){
                
                nSumLenf = (int)((float)(nSumLenf) / fWeight_For_5x5_Sobel_Filter);
                nSumWidf = (int)((float)(nSumWidf) / fWeight_For_5x5_Sobel_Filter);
                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //The sum of absolute values
                if (nSumLenf < 0)
                {
                    nSumOfABS_Of_nSumLenf_and_nSumWidf += (-nSumLenf);
                } //if (nSumLenf < 0)
                else if (nSumLenf > 0)
                {
                    
                    nSumOfABS_Of_nSumLenf_and_nSumWidf += nSumLenf;
                } //else if (nSumLenf > 0)
                
                if (nSumWidf < 0)
                {
                    nSumOfABS_Of_nSumLenf_and_nSumWidf += (-nSumWidf);
                } //if (nSumWidf < 0)
                else if (nSumWidf > 0)
                {
                    
                    nSumOfABS_Of_nSumLenf_and_nSumWidf += nSumWidf;
                } //else if (nSumWidf > 0)
                
                if (USING_ONLY_BLACK_AND_WHITE_COLORS){
                    if (nSumOfABS_Of_nSumLenf_and_nSumWidf > nIntensityThresholdForSobelMax)
                        nSumOfABS_Of_nSumLenf_and_nSumWidf = nIntensityStatMax;
                        
                        if (nSumOfABS_Of_nSumLenf_and_nSumWidf < nIntensityThresholdForSobelMin)
                            nSumOfABS_Of_nSumLenf_and_nSumWidf = 0;
                            
                            } //#ifdef USING_ONLY_BLACK_AND_WHITE_COLORS
                
                if (USE_SUMS_OF_IMAGE_GRADIENTS){
                    
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nSumOfABS_Of_nSumLenf_and_nSumWidf;
                    
                } // #ifdef USE_SUMS_OF_IMAGE_GRADIENTS
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
                if (USE_SUMS_OF_IMAGE_GRADIENTS){
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = (int)(sqrt((float)(nSumLenf*nSumLenf + nSumWidf*nSumWidf) / fNormalizing));
                    //sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = (int)(sqrt( (float)(nSumLenf*nSumLenf + nSumWidf*nSumWidf) ));
                    
                    if (USING_ONLY_BLACK_AND_WHITE_COLORS){
                        if (sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] >= nIntensityThresholdForSobelMax)
                            sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = 0;
                            else
                                sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = 255;
                                } //#ifdef USING_ONLY_BLACK_AND_WHITE_COLORS
                    
                } // #ifndef USE_SUMS_OF_IMAGE_GRADIENTS
                
                // The histogram threshold could be applied here
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
                if (sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] > nIntensityStatMax)
                    sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax;
                    else if (sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] < 0)
                        sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = 0;
                        
                        
                        if (BLACK_BACKGROUND_WHITE_CONTOURS){
                            sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax - sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef];
                        } //#ifndef BLACK_BACKGROUND_WHITE_CONTOURS
                
            } //for (iLenf = 0; iLenf < sOne_Filtered_Imagef->nLenCur; iLenf++)
            
        } //for (iWidf = 0; iWidf < sOne_Filtered_Imagef->nWidCur; iWidf++)

    } // #ifdef DIM5x5_SOBEL_FILTER

    return 1;
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} //int Sobel_Filtering(...

  ///////////////////////////////////////////////////////////////////////////
  //From I.Pitas, Digital Image Processing Algorithms and Applications, 2000, p.33
  // Alterenative: Practical algorithms for image analysis, Michael Seul and others, p.53

int HSI_Colors(
	const ONE_ORIG_IMAGE *sOne_Orig_Image_Redf,
	const ONE_ORIG_IMAGE *sOne_Orig_Image_Greenf,
	const ONE_ORIG_IMAGE *sOne_Orig_Image_Bluef,

	ONE_ORIG_IMAGE *sOne_Orig_Image_Intensityf,
	ONE_ORIG_IMAGE *sOne_Orig_Image_Saturationf,
	ONE_ORIG_IMAGE *sOne_Orig_Image_Huef)
{
	int
		nImageLengthf = sOne_Orig_Image_Redf->nLenCur,
		nImageWidthf = sOne_Orig_Image_Redf->nWidCur,

		nIndexCurSizef, // = iLenf + (iWidf*nImageLengthf);

		nRedCurf,
		nGreenCurf,
		nBlueCurf,

		nColorMinf,

		iLenf,
		iWidf;

	float
		fWidthOfIntensityBinForSaturationf = 1.0 / (float)(nIntensityStatMax),

		fHueNotNormalisedArrf[nImageSizeMax],

		fHueNotNormalisedMinf = fLarge,
		fHueNotNormalisedMaxf = -fLarge,

		fNumerf,
		fDenomf,

		fRatioForHuef,



		fTemp1f,
		fTemp2f,


		fHueNotNormaliseRangef;
	/////////////////////////////////////////////////////////////////////////////////////////////
    if (HUE_BY_1ST_WAY){
        if (HUE_BY_2ND_WAY){

	printf("\n\n An error in 'HSI_Colors': please choose only one way to calulate Hue");
	fprintf(fout, "\n\n An error in 'HSI_Colors': please choose only one way to calulate Hue");

	//printf("\n\n Please press any key to exit");
	//fflush(fout); getchar();
            exit(1);
        } // #ifdef HUE_BY_2ND_WAY

    } // #ifdef HUE_BY_1ST_WAY

	sOne_Orig_Image_Huef->nLenCur = nImageLengthf;
	sOne_Orig_Image_Huef->nWidCur = nImageWidthf;

	sOne_Orig_Image_Saturationf->nLenCur = nImageLengthf;
	sOne_Orig_Image_Saturationf->nWidCur = nImageWidthf;

	sOne_Orig_Image_Intensityf->nLenCur = nImageLengthf;
	sOne_Orig_Image_Intensityf->nWidCur = nImageWidthf;

	//printf("\n\n An error in 'HSI_Colors': please choose only one way to calulate Hue");

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//for (iLenf = 8; iLenf < nImageLengthf; iLenf++)
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexCurSizef = iLenf + (iWidf*nImageLengthf);

			//sOne_Filtered_Image->nPixelArr[nIndexCurSizef]
			nRedCurf = sOne_Orig_Image_Redf->nPixelArr[nIndexCurSizef];
			nGreenCurf = sOne_Orig_Image_Greenf->nPixelArr[nIndexCurSizef];
			nBlueCurf = sOne_Orig_Image_Bluef->nPixelArr[nIndexCurSizef];

			//Intensity first
			sOne_Orig_Image_Intensityf->nPixelArr[nIndexCurSizef] = (nRedCurf + nGreenCurf + nBlueCurf) / 3; //differs from the I.Pitas book

																											 //Saturation (normalized to (0, nIntensityStatMax)
			if (nRedCurf <= nGreenCurf)
			{
				if (nRedCurf <= nBlueCurf)
				{
					nColorMinf = nRedCurf;
				} // if (nRedCurf <= nBlueCurf)
				else
					nColorMinf = nBlueCurf;
			} // if (nRedCurf <= nGreenCurf)
			else if (nRedCurf > nGreenCurf)
			{
				if (nGreenCurf <= nBlueCurf)
				{
					nColorMinf = nGreenCurf;
				} // if (nGreenCurf <= nBlueCurf)
				else
					nColorMinf = nBlueCurf;

			} // else if (nRedCurf > nGreenCurf)


			if (sOne_Orig_Image_Intensityf->nPixelArr[nIndexCurSizef] != 0)
			{
				sOne_Orig_Image_Saturationf->nPixelArr[nIndexCurSizef] = (int)((1.0 - (float)(nColorMinf) / (float)(sOne_Orig_Image_Intensityf->nPixelArr[nIndexCurSizef])) / fWidthOfIntensityBinForSaturationf);

				if (iWidf == nImageWidthf / 2)
					fprintf(fout, "\n iWidf = %d, iLenf = %d, ...->Saturationf->nPixelArr[nIndexCurSizef][%d] = %d", iWidf, iLenf, nIndexCurSizef, sOne_Orig_Image_Saturationf->nPixelArr[nIndexCurSizef]);

			} //if (sOne_Orig_Image_Intensityf->nPixelArr[nIndexCurSizef] != 0)
			else
				sOne_Orig_Image_Saturationf->nPixelArr[nIndexCurSizef] = 0;


			if (sOne_Orig_Image_Saturationf->nPixelArr[nIndexCurSizef] < 0 || sOne_Orig_Image_Saturationf->nPixelArr[nIndexCurSizef] > nIntensityStatMax)
			{

				printf("\n\n An error in 'HSI_Colors': iLenf = %d, iWidf = %d, sOne_Orig_Image_Saturationf->nPixelArr[%d] = %d",
					iLenf, iWidf, nIndexCurSizef, sOne_Orig_Image_Saturationf->nPixelArr[nIndexCurSizef]);

				printf("\n\n nColorMinf = %d, sOne_Orig_Image_Intensityf->nPixelArr[%d] = %d", nColorMinf, nIndexCurSizef, sOne_Orig_Image_Intensityf->nPixelArr[nIndexCurSizef]);

				printf("\n\n nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", nRedCurf, nGreenCurf, nBlueCurf);

				fprintf(fout, "\n\n An error in 'HSI_Colors': iLenf = %d, iWidf = %d, sOne_Orig_Image_Saturationf->nPixelArr[%d] = %d",
					iLenf, iWidf, nIndexCurSizef, sOne_Orig_Image_Saturationf->nPixelArr[nIndexCurSizef]);


				fprintf(fout, "\n\n nColorMinf = %d, sOne_Orig_Image_Intensityf->nPixelArr[%d] = %d", nColorMinf, nIndexCurSizef, sOne_Orig_Image_Intensityf->nPixelArr[nIndexCurSizef]);

				fprintf(fout, "\n\n nRedCurf = %d, nGreenCurf = %d, nBlueCurf = %d", nRedCurf, nGreenCurf, nBlueCurf);

				//printf("\n\n Please press any key to exit");
				//fflush(fout); getchar();
                exit(1);
			} //if (sOne_Orig_Image_Saturationf->nPixelArr[nIndexCurSizef] < 0 || sOne_Orig_Image_Saturationf->nPixelArr[nIndexCurSizef] > nIntensityStatMax)

			  // Hue (not yet normalized to (0, nIntensityStatMax)

            if (HUE_BY_1ST_WAY){
			fTemp1f = 0.7071*((float)(nGreenCurf)-(float)(nBlueCurf));

			fTemp2f = 0.816496*(float)(nRedCurf)-0.40824*((float)(nGreenCurf)+(float)(nBlueCurf));

			if (fTemp2f < feps && fTemp2f > -feps)
				fHueNotNormalisedArrf[nIndexCurSizef] = pi / 2.0;
			else
				fHueNotNormalisedArrf[nIndexCurSizef] = atan2(fTemp1f, fTemp2f);
            } // #ifdef HUE_BY_1ST_WAY

            if (HUE_BY_2ND_WAY){

			fNumerf = (float)((nRedCurf - nGreenCurf) + (nRedCurf - nBlueCurf));

			fDenomf = 2.0*(sqrt((double)((nRedCurf - nGreenCurf)*(nRedCurf - nGreenCurf) + (nRedCurf - nBlueCurf)*(nGreenCurf - nBlueCurf))));

			if (fDenomf < feps && fDenomf > -feps)
				fHueNotNormalisedArrf[nIndexCurSizef] = 0.0;
			else
			{
				fRatioForHuef = fNumerf / fDenomf;

				if (fRatioForHuef < -1.0)
					fRatioForHuef = -1.0;

				if (fRatioForHuef > 1.0)
					fRatioForHuef = 1.0;

				fHueNotNormalisedArrf[nIndexCurSizef] = acosf(fRatioForHuef);
			} //
            } // #ifdef HUE_BY_2ND_WAY

			if (fHueNotNormalisedArrf[nIndexCurSizef] < fHueNotNormalisedMinf)
				fHueNotNormalisedMinf = fHueNotNormalisedArrf[nIndexCurSizef];

			if (fHueNotNormalisedArrf[nIndexCurSizef] > fHueNotNormalisedMaxf)
				fHueNotNormalisedMaxf = fHueNotNormalisedArrf[nIndexCurSizef];

		} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	} //for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	  //Normalization of Hue to (0, nIntensityStatMax)
	fHueNotNormaliseRangef = fHueNotNormalisedMaxf - fHueNotNormalisedMinf;
	if (fHueNotNormaliseRangef < 0.0 || fHueNotNormaliseRangef > fLarge)
	{
		printf("\n\n An error in 'HSI_Colors': fHueNotNormaliseRangef = %E < 0.0 || ...", fHueNotNormaliseRangef);
		fprintf(fout, "\n\n An error in 'HSI_Colors': fHueNotNormaliseRangef = %E < 0.0 || ...", fHueNotNormaliseRangef);

		//printf("\n\n Please press any key to exit");
		//fflush(fout); getchar();
        exit(1);
	} //if (fHueNotNormaliseRangef < 0.0 || fHueNotNormaliseRangef > fLarge)

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		fprintf(fout, "\n\n 'HSI_Colors': iWidf = %d", iWidf);
		//for (iLenf = 8; iLenf < nImageLengthf; iLenf++)
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexCurSizef = iLenf + (iWidf*nImageLengthf);

			sOne_Orig_Image_Huef->nPixelArr[nIndexCurSizef] = (int)(((fHueNotNormalisedArrf[nIndexCurSizef] - fHueNotNormalisedMinf) / fHueNotNormaliseRangef)*(float)(nIntensityStatMax));
			
			fprintf(fout, "\n iLenf = %d, iWidf = %d, sOne_Orig_Image_Huef->nPixelArr[%d] = %d",
									iLenf, iWidf, nIndexCurSizef, sOne_Orig_Image_Huef->nPixelArr[nIndexCurSizef]);

			if (sOne_Orig_Image_Huef->nPixelArr[nIndexCurSizef] < 0 || sOne_Orig_Image_Huef->nPixelArr[nIndexCurSizef] > nIntensityStatMax)
			{

				printf("\n\n An error in 'HSI_Colors': iLenf = %d, iWidf = %d, sOne_Orig_Image_Huef->nPixelArr[%d] = %d",
					iLenf, iWidf, nIndexCurSizef, sOne_Orig_Image_Huef->nPixelArr[nIndexCurSizef]);

				fprintf(fout, "\n\n An error in 'HSI_Colors': iLenf = %d, iWidf = %d, sOne_Orig_Image_Huef->nPixelArr[%d] = %d",
					iLenf, iWidf, nIndexCurSizef, sOne_Orig_Image_Huef->nPixelArr[nIndexCurSizef]);

				//printf("\n\n Please press any key to exit");
				//fflush(fout); getchar();
                exit(1);
			} // if (sOne_Orig_Image_Huef->nPixelArr[nIndexCurSizef] < 0 || sOne_Orig_Image_Huef->nPixelArr[nIndexCurSizef] > nIntensityStatMax)

		} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	} //for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return 1;
} // int HSI_Colors(...




  //printf("\n\n Please press any key:"); getchar();
