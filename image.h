#ifndef image_hpp
#define image_hpp
#include <array>
#include <string>
#include <math.h>

namespace imago
{
    typedef unsigned char uchar;

    typedef std::array<float, 9> Kern3x3;
    typedef std::array<float, 25> Kern5x5;
 
    template<typename T>
    uchar clip(T val)
    {
        return (val < 0) ? 0 : (val > 255) ? 255 : val;
    }

    struct RGB
    {
        uchar r;
        uchar g;
        uchar b;

        RGB() : r(0), g(0), b(0)
        {}

        RGB(uchar rr, uchar gg, uchar bb)
            : r(rr), g(gg), b(bb)
        {}

        double distance(const RGB& other)
        {
            double rd = double(r)-other.r;
            double gd = double(g)-other.g;
            double bd = double(b)-other.b;

            return sqrt(rd*rd+gd*gd+bd*bd);
        }

        // Returns luminance calculated by ITU BT.601 digital luma coefficients
        uchar luminance() const
        {
            double res = floor(0.299*r + 0.587*g + 0.114*b + 0.5);
            return clip(res);
        }
    };
   
    typedef std::array<unsigned int, 256> Histogram;

    struct Stats
    {
        Stats();
        Histogram hist;
        double mean() const;
        double median() const;
        double std() const;
    };

    /** Image - an RGBA image with move semantics
     */
    class Image
    {
        uchar* _data;
        int    _width;
        int    _height;
        int    _pitchInBytes;

        Image(const Image&); // no copy ctor

     public:
        enum Color
        {
            B = 0,
            G = 1,
            R = 2,
            A = 3
        };

        Image();
        ~Image();
        Image(int w, int h, int pitchInBytes = 0);

        Image(Image&& other);
        Image& operator=(const Image& other);
        Image& operator=(Image&& other);

        uchar operator()(int row, int col, Color color) const;
        uchar& operator()(int row, int col, Color color);


        int width() const;
        int height() const;
        int pitchInBytes() const;
        uchar* data() const;

        /** Load/save image (expects utf8 encoded string) */
        bool read(const std::string& filename);
        bool write(const std::string& filename) const;

        Image resize(int w, int h) const;
        void copyTo(Image& other) const;

        Stats getStats(Color color) const;
    };

    // Plane - uchar Plane with move semantics
    class Plane
    {
        uchar* _data;
        int    _width;
        int    _height;
        int    _pitchInBytes;

        Plane(const Image&); // no copy ctor
        Plane& operator=(const Plane& other);

     public:
        Plane();
        ~Plane();
        Plane(int w, int h, int pitchInBytes = 0);
        Plane(Plane&& other);

        Plane(const Image& in, Image::Color channel);
        void toImage(Image& out, Image::Color channel);

        Plane& operator=(Plane&& other);

        uchar operator()(int row, int col) const;
        uchar& operator()(int row, int col);


        int width() const;
        int height() const;
        int pitchInBytes() const;
        uchar* data() const;

        Stats getStats() const;
    };

    const Image::Color R = Image::R;
    const Image::Color G = Image::G;
    const Image::Color B = Image::B;
    const Image::Color A = Image::A;
}
#endif
