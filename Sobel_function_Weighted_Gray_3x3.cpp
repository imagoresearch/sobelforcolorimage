//#include "Sobel_Parameters_Weighted_Gray_3x3.h"

#include "Sobel_Test_Color_Weighted_Gray_3x3_2.h"

using namespace imago;

FILE *fout;

/*
#define fFadingFactor 1.0

#define fWeight_Red 80.0 //(100.0) //(100.0/3.0)


#define fWeight_Green 10.0 //20.0 //(100.0/2.0)


#define fWeight_Blue 10.0 //(100.0/2.0)
*/

int doSobel_Weighted_Gray_3x3(
	const char *inputFileNamef,

	const char *outputFileNamef,
	//const bool bINCLUDE_WEIGHTED_GRAYf,

	const PARAMETERS_WEIGHTED_GRAY_3x3 *sParameters_Weighted_Gray_3x3f)

{
	int Initializing_One_Orig_Image_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		ONE_ORIG_IMAGE *sOne_Orig_Imagef);	

	int Histogram_Statistics_For_Orig_Image(

		ONE_ORIG_IMAGE *sOne_Orig_Imagef, //[nImageSizeMax]

		int &nObjectAreaf,
		float &fPercentageOfobjectAreaf,
		int nHistogramArrf[], //[nNumOfHistogramBinsStat]
		float fPercentagesInHistogramArrf[]);//[nNumOfHistogramBinsStat]

	int Sobel_Filtering(
			const PARAMETERS_WEIGHTED_GRAY_3x3 *sParameters_Weighted_Gray_3x3f,
		const ONE_ORIG_IMAGE *sOne_Orig_Imagef,
		ONE_ORIG_IMAGE *sOne_Filtered_Imagef);

	int 
		iWid,
		iLen,

	//////////////////////
		nImageWidth, // is actually 'nImageLength' comparing with the "Cimage" implementation.
		nImageLength,
		
		nObjectArea,
		nHistogramArr[nNumOfHistogramBinsStat], //[nNumOfHistogramBinsStat]

		iHistogramBin,

		nIndexCurSize,
		nNumOfPixelsAboveIntensityThreshold = 0,
		nRes;

	float
		fSumOfPercentsf,
		fDeviationOfSumFrom_100percentsf,

		fPercentageOfobjectArea,
		fPercentagesInHistogramArr[nNumOfHistogramBinsStat]; //[nNumOfHistogramBinsStat]

	fout = fopen("wMain_Sobel_Color.txt", "w");

	if (fout == NULL)
	{
		printf("\n\n fout == NULL");
		getchar(); exit(1);
	} //if (fout == NULL)
	
//////////////////////////////////////////////////////////////////////////
	

	fSumOfPercentsf = sParameters_Weighted_Gray_3x3f->fWeight_Redf + sParameters_Weighted_Gray_3x3f->fWeight_Greenf + sParameters_Weighted_Gray_3x3f->fWeight_Bluef;
	fDeviationOfSumFrom_100percentsf = 100.0 - fSumOfPercentsf;

	if (fDeviationOfSumFrom_100percentsf < 0.0)
		fDeviationOfSumFrom_100percentsf = -fDeviationOfSumFrom_100percentsf;

	if (fDeviationOfSumFrom_100percentsf > fDeviationFrom_100percentsMax)
	{
		printf("\n\n An error: the sum of the color weights deviates too much from 100 percents");
		printf("\n\n Please adjust the color weights");

		//printf("\n\n Please press any key to exit:");
		//getchar();
        exit(1);
	} //if (fDeviationOfSumFrom_100percentsf > fDeviationFrom_100percentsMax)

 ///////////////////////////////////////////////////////////////////////////

	Image testImg;

	//testImg.read("ButterFly_Test_Image.jpg");
	//testImg.read("inputFileNamef");
	testImg.read(inputFileNamef);

	// size of image
	//nImageWidth = testImg.width();
	//nImageHeight = testImg.height();

	 nImageWidth = testImg.height();  
	nImageLength = testImg.width();

	printf("\n nImageWidth = %d, nImageLength = %d", nImageWidth, nImageLength);

	if (nImageLength > nLenMax || nImageLength < nLenMin)
	{
		printf("\n\n An error in reading the image width: nImageLength = %d", nImageLength);
		//printf("\n\n Please press any key to exit");
		//getchar();
        exit(1);

	} //if (nImageLength > nLenMax || nImageLength < nLenMin)

	if (nImageWidth > nWidMax || nImageWidth < nWidMin)
	{
		printf("\n\n An error in reading the image height: nImageWidth = %d", nImageWidth);
		//printf("\n\n Please press any key to exit");
		//getchar();
        exit(1);

	} // if (nImageWidth > nWidMax || nImageWidth < nWidMin)

	//printf("\n\n Please press any key:"); getchar();

	if (nIntensityThresholdForDense <= nIntensityStatMin || nIntensityThresholdForDense >= nIntensityStatMax || nIntensityThresholdForDense < 1 || nIntensityStatMin < 1)
	{
		printf("\n\n An error in specification of intensity thresholds: nIntensityThresholdForDense = %d, nIntensityStatMin = %d",
			nIntensityThresholdForDense, nIntensityStatMin);

		printf("\n\n The intensity threshold for the dense area 'nIntensityThresholdForDense' should be larger than the threshold for the object area 'nIntensityStatMin'");

		//printf("\n\n Please press any key to exit:");
		//getchar();
        exit(1);
	} // if (nIntensityThresholdForDense <= nIntensityStatMin || nIntensityThresholdForDense >= nIntensityStatMax)

	//////////////////////////////////////////////////////////////////////////

	ONE_ORIG_IMAGE
		sOne_Orig_Image_WeightedGray;

	nRes = Initializing_One_Orig_Image_To_CurSize(
		nImageWidth, //const int nImageWidthf,
		nImageLength, //const int nImageLengthf,

		&sOne_Orig_Image_WeightedGray); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]


	// use 'nIntensityThreshold' to change the number of object pixels!

	int
		nIntensity_Read_Test_ImageMax = -nLarge,
		nRed,
		nGreen,
		nBlue;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (iWid = 0; iWid < nImageWidth; iWid++) //j
	{
		for (iLen = 0; iLen < nImageLength; iLen++) //i
		{

			//nIndexCurSize = i + iWid*nImageLength;

			nRed = testImg(iWid, iLen, R);

			nGreen = testImg(iWid, iLen, G);
			nBlue = testImg(iWid, iLen, B);

			if (nRed < 0 || nRed > nIntensityStatMax || nGreen < 0 || nGreen > nIntensityStatMax || nBlue < 0 || nBlue > nIntensityStatMax)
			{
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, iWid = %d, iLen = %d", nRed, nGreen, nBlue, iWid,iLen);
				//printf("\n\n Please press any key to exit");
				//getchar();
                exit(1);
			} // if (nRed < 0 || nRed > nIntensityStatMax || nGreen < 0 || nGreen > nIntensityStatMax || nBlue < 0 || nBlue > nIntensityStatMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

			//fprintf(fout, "\n nPixelArr[%d] = %d, iLen = %d, iWid = %d", nIndexOfPixel, nPixelArr[nIndexOfPixel], iLen, iWid);

		} // for (iLen = 0; iLen < nImageLength; iLen++)

	}//for (iWid = 0; iWid < nImageWidth; iWid++)

	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);

	fprintf(fout, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	////////////////////////////////////////////////////////////////////////////////////////////

	for (iWid = 0; iWid < nImageWidth; iWid++) //j
	{
		for (iLen = 0; iLen < nImageLength; iLen++) //i
		{

			nIndexCurSize = iLen + iWid*nImageLength;

			nRed = testImg(iWid, iLen, R);
			nGreen = testImg(iWid, iLen, G);
			nBlue = testImg(iWid, iLen, B);
	
				if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling
				nRed = nRed / 256;

				nGreen = nGreen / 256;
				nBlue = nBlue / 256;
			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
	

			 //reassigning
			//sOne_Orig_Image_WeightedGray.nPixelArr[nIndexMaxSize] = 
					//(sOne_Orig_Image_Red.nPixelArr[nIndexMaxSize] + sOne_Orig_Image_Green.nPixelArr[nIndexMaxSize] + sOne_Orig_Image_Blue.nPixelArr[nIndexMaxSize]) / 3;

			  //Converting to decimals
			sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize] = (int)(sParameters_Weighted_Gray_3x3f->fFadingFactorf*(sParameters_Weighted_Gray_3x3f->fWeight_Redf*(float)(nRed) +
				sParameters_Weighted_Gray_3x3f->fWeight_Greenf*(float)(nGreen) +
				sParameters_Weighted_Gray_3x3f->fWeight_Bluef*(float)(nBlue)) / 100.0);

			if (sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize] > nIntensityStatMax)
			{
				sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize] = nIntensityStatMax;

			} //if (sOne_Orig_Image_Red.nPixelArr[nIndexMaxSize] > nIntensityStatMin)

			///////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize] > nIntensityStatMin)
			{
				nNumOfPixelsAboveIntensityThreshold += 1;
				//fprintf(fout, "\n\n A new nNumOfPixelsAboveIntensityThreshold = %d, iWid = %d, iLen = %d, sOne_Orig_Image_WeightedGray.nPixelArr[%d] = %d", 
				//nNumOfPixelsAboveIntensityThreshold,iWid, iLen, nIndexMaxSize, sOne_Orig_Image_WeightedGray.nPixelArr[nIndexMaxSize]);

			} //if (sOne_Orig_Image_WeightedGray.nPixelArr[nIndexMaxSize] > nIntensityStatMin)

/*
			if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
			{
				fprintf(fout, "\n\n An initial subimage iWid = %d, iLen = %d, sOne_Orig_Image_WeightedGray.nPixelArr[%d] = %d", 
					iWid, iLen, nIndexCurSize, sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize]);

				fprintf(fout, "\n nRed = %d, nGreen = %d, nBlue = %d, Gray = %d", nRed, nGreen, nBlue, sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize]);

			} // if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
*/
		
		} // for (iLen = 0; iLen < nImageLength; iLen++) //i

	}//for (iWid = 0; iWid < nImageWidth; iWid++) //j

	 ///////////////////////////////////////////////////////////////////////////

	  //  printf("\n\n After reading the image: please press any key to exit"); fflush(fout);  getchar(); exit(1);
	//printf("\n\n After reading the image: please press any key to continue"); fflush(fout);  getchar();

	///////////////////////////////////
	nRes = Histogram_Statistics_For_Orig_Image(
		&sOne_Orig_Image_WeightedGray, // ONE_ORIG_IMAGE *sOne_Orig_Imagef, //[nImageSizeMax]

		nObjectArea, //int &nObjectAreaf,

		fPercentageOfobjectArea, //float &fPercentageOfobjectAreaf,
		nHistogramArr, //int nHistogramArrf[], //[nNumOfHistogramBinsStat]
		fPercentagesInHistogramArr); // float fPercentagesInHistogramArrf[]);//[nNumOfHistogramBinsStat]

	printf("\n\n Gray conversion: nObjectArea = %d, fPercentageOfobjectArea = %E \n", nObjectArea, fPercentageOfobjectArea);
	fprintf(fout, "\n\n Gray conversion: nObjectArea = %d, fPercentageOfobjectArea = %E \n", nObjectArea, fPercentageOfobjectArea);

	for (iHistogramBin = 0; iHistogramBin < nNumOfHistogramBinsStat; iHistogramBin++)
	{
		printf("\n Gray: nHistogramArr[%d] = %d, fPercentagesInHistogramArr[%d] = %E",
			iHistogramBin, nHistogramArr[iHistogramBin], iHistogramBin, fPercentagesInHistogramArr[iHistogramBin]);

		fprintf(fout, "\n Gray: nHistogramArr[%d] = %d, fPercentagesInHistogramArr[%d] = %E",
			iHistogramBin, nHistogramArr[iHistogramBin], iHistogramBin, fPercentagesInHistogramArr[iHistogramBin]);
	} //for (iHistogramBin = 0; iHistogramBin < nNumOfHistogramBinsStat; iHistogramBin++)

	  //printf("\n\n After 'Histogram_Statistics_For_Orig_Image()': please press any key to exit"); fflush(fout); getchar(); exit(1);
	//printf("\n\n After 'Histogram_Statistics_For_Orig_Image()' Gray conversion: please press any key"); fflush(fout); getchar(); //exit(1);

																																 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	ONE_ORIG_IMAGE sOne_Filtered_Image;

	nRes = Initializing_One_Orig_Image_To_CurSize(
		nImageWidth, //const int nImageWidthf,
		nImageLength, //const int nImageLengthf,

		&sOne_Filtered_Image); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]
	
		nRes = Sobel_Filtering(
				sParameters_Weighted_Gray_3x3f, //const PARAMETERS_WEIGHTED_GRAY_3x3 *sParameters_Weighted_Gray_3x3f,
			&sOne_Orig_Image_WeightedGray, //const ONE_ORIG_IMAGE *sOne_Orig_Imagef,
			&sOne_Filtered_Image); // ONE_ORIG_IMAGE *sOne_Filtered_Image)

 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//printf("\n\n Before drawing pixels of edges: nImageWidth = %d, nImageLength = %d, please press any key", nImageWidth, nImageLength); getchar();

	int bytesOfWidth = testImg.pitchInBytes();

	int nStep = testImg.pitchInBytes() / (testImg.width() * sizeof(unsigned char));

	// Save to file
	Image imageToSave(nImageLength, nImageWidth, bytesOfWidth);

	//initial shading of the image 
	for (iWid = 0; iWid < nImageWidth; iWid++) //j
	{
/*
#ifdef PRINTING_SUBIMAGE

		if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax)
		{
			fprintf(fout, "\n\n Filtered: iWid = %d\n", iWid);
		} // if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax)

#endif // #ifdef PRINTING_SUBIMAGE
*/
		for (iLen = 0; iLen < nImageLength; iLen++) //i
		{
			nIndexCurSize = iLen + (iWid*nImageLength);

		//	fprintf(fout, "\n Filtered: iWid = %d, iLen = %d, sOne_Filtered_Image.nPixelArr[%d] = %d", iWid, iLen, nIndexCurSize, sOne_Filtered_Image.nPixelArr[nIndexCurSize]);
/*
#ifdef PRINTING_SUBIMAGE
			if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
			{
				fprintf(fout, "\n iWid = %d, iLen = %d, sOne_Filtered_Image.nPixelArr[%d] = %d, sOne_Orig_Image_WeightedGray.nPixelArr[%d] = %d",
					iWid, iLen, nIndexCurSize, sOne_Filtered_Image.nPixelArr[nIndexCurSize], nIndexCurSize, sOne_Orig_Image_WeightedGray.nPixelArr[nIndexCurSize]);

			} // if (iWid >= nWidSubimageToPrintMin && iWid <= nWidSubimageToPrintMax && iLen >= nLenSubimageToPrintMin && iLen <= nLenSubimageToPrintMax)
#endif // #ifdef PRINTING_SUBIMAGE
*/


if (sParameters_Weighted_Gray_3x3f->bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true) {
			if (sOne_Filtered_Image.nPixelArr[nIndexCurSize] < nIntensityStatMin)
			{
				//GrayByColorArr[0] = 0; // 1 * nGrayLevelIntensityForDenseArea; //black color for contours
				//GrayByColorArr[1] = 0; // 1 * nGrayLevelIntensityForDenseArea;
				//GrayByColorArr[2] = 0; // 1 * nGrayLevelIntensityForDenseArea;
				imageToSave(iWid, iLen, R) = 0;
				imageToSave(iWid, iLen, G) = 0;
				imageToSave(iWid, iLen, B) = 0;

			} // if (sOne_Orig_Image.nPixelArr[nIndexCurSize] < nIntensityStatMin)
			else if (sOne_Filtered_Image.nPixelArr[nIndexCurSize] >= nIntensityStatMin)
			{
			//	GrayByColorArr[0] = nIntensityStatMax; // 1 * nGrayLevelIntensityToDrawContours; //white color for the remainder
				//GrayByColorArr[1] = nIntensityStatMax; // 1 * nGrayLevelIntensityToDrawContours;
				//GrayByColorArr[2] = nIntensityStatMax; // 1 * nGrayLevelIntensityToDrawContours;

				imageToSave(iWid, iLen, R) = nIntensityStatMax;
				imageToSave(iWid, iLen, G) = nIntensityStatMax;
				imageToSave(iWid, iLen, B) = nIntensityStatMax;
			} // else if (sOne_Orig_Image.nPixelArr[nIndexCurSize] >= nIntensityStatMin)
} // if (sParameters_Weighted_Gray_3x3f->bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true) {


			if (sParameters_Weighted_Gray_3x3f->bUSING_ONLY_BLACK_AND_WHITE_COLORSf == false) {

				//GrayByColorArr[0] = sOne_Filtered_Image.nPixelArr[nIndexCurSize];
				//GrayByColorArr[1] = sOne_Filtered_Image.nPixelArr[nIndexCurSize];
				//GrayByColorArr[2] = sOne_Filtered_Image.nPixelArr[nIndexCurSize];

				//imageToSave(j, i, R) = imageToSave(j, i, G) = imageToSave(j, i, B) = sGrayscale_Image.nGrayscale_Arr[nIndexOfPixelMax];
				imageToSave(iWid, iLen, R) = sOne_Filtered_Image.nPixelArr[nIndexCurSize];
				imageToSave(iWid, iLen, G) = sOne_Filtered_Image.nPixelArr[nIndexCurSize];
				imageToSave(iWid, iLen, B) = sOne_Filtered_Image.nPixelArr[nIndexCurSize];

			} //if (sParameters_Weighted_Gray_3x3f->bUSING_ONLY_BLACK_AND_WHITE_COLORSf == false) {

			imageToSave(iWid, iLen, A) = testImg(iWid, iLen, A);
		} //for (iLen = 0; iLen < nLength; iLen++)

	} //for (iWid = 0; iWid < nImageWidth; iWid++)

////////////////////////////////////////////////////////////////////////////////
	//imageToSave.write("ImageSaved_Gray.png");

	imageToSave.write(outputFileNamef);

	printf("\n\n The grayscale image has been saved");

	//printf("\n\n After drawing the image: please press any key to exit"); fflush(fout);  getchar();  exit(1);

/////////////////////////////////////////////////////////////////////////////////
	delete[] sOne_Orig_Image_WeightedGray.nPixelArr;

	delete[] sOne_Filtered_Image.nPixelArr;
  /////////////////////////////////////////////////////////////////////////////////
	
	fclose(fout);
	return 1;
} //int doSobel_Weighted_Gray_3x3(...)

  /////////////////////////////////////////////////////////////////////////////////////////////////////
int Histogram_Statistics_For_Orig_Image(

	ONE_ORIG_IMAGE *sOne_Orig_Imagef, //[nImageSizeMax]

	int &nObjectAreaf,
	float &fPercentageOfobjectAreaf,
	int nHistogramArrf[], //[nNumOfHistogramBinsStat]
	float fPercentagesInHistogramArrf[]) //[nNumOfHistogramBinsStat]
{
	int
		nImageWidthf = sOne_Orig_Imagef->nWidCur,
		nImageLengthf = sOne_Orig_Imagef->nLenCur,

		nAreaOfTheWholeImagef,
		nWidthOfABinInHistogramf = (nIntensityStatMax - nIntensityStatMin) / nNumOfHistogramBinsStat,

		nNumOfHistogramBinCurf,

		nIntensityCurf,
		nIndexCurSizef,

		iHistogramBinf,
		iLenf,
		iWidf;

	if (nImageLengthf < 1 || nImageLengthf > nLenMax || nImageWidthf < 1 || nImageWidthf > nWidMax)
	{
		printf("\n\n An error in 'Histogram_Statistics_For_Orig_Image': nImageLengthf = %d, nLenMax = %d, nImageWidthf = %d, nWidMax = %d",
			nImageLengthf, nLenMax, nImageWidthf, nWidMax);

		fprintf(fout, "\n\n An error in 'Histogram_Statistics_For_Orig_Image': nImageLengthf = %d, nLenMax = %d, nImageWidthf = %d, nWidMax = %d",
			nImageLengthf, nLenMax, nImageWidthf, nWidMax);

		//printf("\n\n Please press any key to exit:");
		//fflush(fout); getchar();
        exit(1);
	} // if (nImageLengthf < 1 || nImageLengthf > nLenMax || nImageWidthf < 1 || nImageWidthf > nWidMax)

	  /////////////////////////////////////////////////

	nAreaOfTheWholeImagef = nImageWidthf*nImageLengthf;

	if (nAreaOfTheWholeImagef <= 0)
	{
		printf("\n\n An error in 'Histogram_Statistics_For_Orig_Image': nAreaOfTheWholeImagef = %d <= 0", nAreaOfTheWholeImagef);

		fprintf(fout, "\n\n An error in 'Histogram_Statistics_For_Orig_Image': nAreaOfTheWholeImagef = %d <= 0", nAreaOfTheWholeImagef);
		//printf("\n\n Please press any key to exit:");
		//fflush(fout); getchar();
        exit(1);
	} // if (nAreaOfTheWholeImagef <= 0)

	nObjectAreaf = 0;

	//memset(nHistogramArrf, 0, sizeof(nHistogramArrf));
	//memset(fPercentagesInHistogramArrf, 0.0, sizeof(fPercentagesInHistogramArrf));

	for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)
	{
		nHistogramArrf[iHistogramBinf] = 0;
		fPercentagesInHistogramArrf[iHistogramBinf] = 0.0;

	} //for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)

	  //////////////////////////////////////////////////////
	  // 
	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		//fprintf(fout, "\n\n iWidf = %d: ", iWidf);

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexCurSizef = iLenf + (iWidf*nImageLengthf);

			nIntensityCurf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizef];

			//fprintf(fout, " %d:%d %d,", iWidf, iLenf, nIntensityCurf);

	
			if (nIntensityCurf >= nIntensityStatMin)
			{
				nObjectAreaf += 1;

				nNumOfHistogramBinCurf = (nIntensityCurf - nIntensityStatMin) / nWidthOfABinInHistogramf;

				if (nNumOfHistogramBinCurf >= nNumOfHistogramBinsStat)
					nNumOfHistogramBinCurf = nNumOfHistogramBinsStat - 1;

				nHistogramArrf[nNumOfHistogramBinCurf] += 1;
			} //if (nIntensityCurf >= nIntensityStatMin)

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)

	} //for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	fPercentageOfobjectAreaf = (float)(nObjectAreaf) / (float)(nAreaOfTheWholeImagef);

	for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)
	{
		if (nObjectAreaf > 0)
		{
			fPercentagesInHistogramArrf[iHistogramBinf] = (float)(nHistogramArrf[iHistogramBinf]) / (float)(nObjectAreaf);
		} //if (nObjectAreaf > 0)
		else if (nObjectAreaf == 0)
		{
			printf("\n\n An error in 'Histogram_Statistics_For_Orig_Image': nObjectAreaf == 0");
			printf("\n Please try decreasing 'nIntensityStatMin'");

			fprintf(fout, "\n\n An error in 'Histogram_Statistics_For_Orig_Image': nObjectAreaf == 0");
			fprintf(fout, "\n Please try decreasing 'nIntensityStatMin'");

			//printf("\n\n Please press any key to exit:");
			//fflush(fout); getchar();
            exit(1);
		} // else if (nObjectAreaf == 0)

	} //for (iHistogramBinf = 0; iHistogramBinf < nNumOfHistogramBinsStat; iHistogramBinf++)

	//sOne_Orig_Imagef->nAreaForStat = nObjectAreaf;

	//sOne_Orig_Imagef->fPercentageOfobjectArea = fPercentageOfobjectAreaf;

	return 1;
} //Histogram_Statistics_For_Orig_Image(...

  //////////////////////////////////////////////////////////////////////

int Initializing_One_Orig_Image_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	ONE_ORIG_IMAGE *sOne_Orig_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf*nImageLengthf,
		iWidf,
		iLenf;

	sOne_Orig_Imagef->nWidCur = nImageWidthf;
	sOne_Orig_Imagef->nLenCur = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];
	sOne_Orig_Imagef->nPixelArr = new int[nImageSizeCurf];

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sOne_Orig_Imagef->nPixelArr[nIndexOfPixelCurf] = -1;

		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nWidMax; iWidf++)

	return 1;
} //int Initializing_One_Orig_Image_To_CurSize(...

  //////////////////////////////////////////////////////////////////////////////////////
int Sobel_Filtering(

		const PARAMETERS_WEIGHTED_GRAY_3x3 *sParameters_Weighted_Gray_3x3f,
	const ONE_ORIG_IMAGE *sOne_Orig_Imagef,
	ONE_ORIG_IMAGE *sOne_Filtered_Imagef)
{
	int Initializing_One_Orig_Image_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		ONE_ORIG_IMAGE *sOne_Orig_Imagef);	//[]

	int

		nIndexCurSizef,
		nIndexCurSizeOfANeighborf,

		nSumLenf,
		nSumWidf,

		nLenNeighf,
		nWidNeighf,

		nImageWidthf = sOne_Orig_Imagef->nWidCur,
		nImageLengthf = sOne_Orig_Imagef->nLenCur,

		nIntensityNeighf,

		nSumOfABS_Of_nSumLenf_and_nSumWidf = 0,
		nResf,
		iWidf,
		iLenf;

nResf = Initializing_One_Orig_Image_To_CurSize(
	nImageWidthf, //const int nImageWidthf,
		nImageLengthf, //const int nImageLengthf,

		sOne_Filtered_Imagef); // ONE_ORIG_IMAGE *sOne_Orig_Imagef);
	
	//3x3 Sobel filter
//#ifdef DIM3x3_SOBEL_FILTER

	// use 'nIntensityThreshold' to change the number of object pixels!
	for (iWidf = 0; iWidf < sOne_Filtered_Imagef->nWidCur; iWidf++)
	{
		fprintf(fout, "\n\n 'Sobel_Filtering': iWidf = %d", iWidf);

		for (iLenf = 0; iLenf < sOne_Filtered_Imagef->nLenCur; iLenf++)
		{
			nIndexCurSizef = iLenf + (iWidf*nImageLengthf);

			if (iWidf == 0 || iWidf == sOne_Filtered_Imagef->nWidCur - 1)
			{
				sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax;
				continue;
			} // if (iWidf == 0 || iWidf == sOne_Filtered_Imagef->nWidCur - 1)

			if (iLenf == 0 || iLenf == sOne_Filtered_Imagef->nLenCur - 1)
			{
				sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax;
				continue;
			} // if (iLenf == 0 || iLenf == sOne_Filtered_Imagef->nLenCur - 1)

			nSumLenf = 0;
			nSumWidf = 0;

			nSumOfABS_Of_nSumLenf_and_nSumWidf = 0;
			/////////////////////////////////////////////////////////////////////
			//1 -- addition for nSumLenf
			nLenNeighf = iLenf - 1;
			nWidNeighf = iWidf + 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
			nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];

			//bVERSION_2_OF_SOBEL_3x3f = true
			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)
			{ 
						nSumLenf += nIntensityNeighf;

						nSumWidf -= nIntensityNeighf;
			} // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) {
						nSumLenf += 3 * nIntensityNeighf;

						nSumWidf -= 3 * nIntensityNeighf;
			} // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) 

			//fprintf(fout, "\n\n 1: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);
			
			//fprintf(fout, "\n nWidNeighf = %d, nLenNeighf = %d, sOne_Orig_Imagef->nPixelArr[%d] = %d",
				//nWidNeighf, nLenNeighf, nIndexCurSizeOfANeighborf,sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf]);

			/////////////////////////////////////////////////////////////////////
			//2 -- addition for nSumLenf
			nLenNeighf = iLenf;
			nWidNeighf = iWidf + 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
			nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)
			{
						nSumLenf += 2 * nIntensityNeighf;
			}  // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)


			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) {
						nSumLenf += 10 * nIntensityNeighf;
			} // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) 

			//fprintf(fout, "\n 2: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//3 -- addition for nSumLenf
			nLenNeighf = iLenf + 1;
			nWidNeighf = iWidf + 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
			nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)
			{
						nSumLenf += nIntensityNeighf;

						nSumWidf += nIntensityNeighf;
			}  // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)


			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) {
						nSumLenf += 3 * nIntensityNeighf;

						nSumWidf += 3 * nIntensityNeighf;
			} // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) 

			//fprintf(fout, "\n 3: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//4 -- subtraction for nSumLenf

			nLenNeighf = iLenf - 1;
			nWidNeighf = iWidf - 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
			nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)
			{
						nSumLenf -= nIntensityNeighf;
						nSumWidf -= nIntensityNeighf;
			}  // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) {
						nSumLenf -= 3 * nIntensityNeighf;

						nSumWidf -= 3 * nIntensityNeighf;
			} // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) 

			//fprintf(fout, "\n 4: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//5 -- subtraction for nSumLenf
			nLenNeighf = iLenf;
			nWidNeighf = iWidf - 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
			nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)
			{
						nSumLenf -= 2 * nIntensityNeighf;
			}  // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) {
						nSumLenf -= 10 * nIntensityNeighf;
			} // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) 

			//fprintf(fout, "\n 5: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//6 -- subtraction for nSumLenf
			nLenNeighf = iLenf + 1;
			nWidNeighf = iWidf - 1;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
			nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)
			{
						nSumLenf -= nIntensityNeighf;

						nSumWidf += nIntensityNeighf;
			}  // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) {
						nSumLenf -= 3 * nIntensityNeighf;

						nSumWidf += 3 * nIntensityNeighf;
			} // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) 

			//fprintf(fout, "\n 6: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//7 -- addition  for nSumWidf
			nLenNeighf = iLenf + 1;
			nWidNeighf = iWidf;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
			nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)
			{
						//addition  for nSumWidf
						nSumWidf += 2 * nIntensityNeighf;
			}  // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) {
						//addition  for nSumWidf
						nSumWidf += 10 * nIntensityNeighf;
			} // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) 

		//	fprintf(fout, "\n 7: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			/////////////////////////////////////////////////////////////////////
			//8 -- subtraction for nSumWidf
			nLenNeighf = iLenf - 1;
			nWidNeighf = iWidf;

			nIndexCurSizeOfANeighborf = nLenNeighf + (nWidNeighf*nImageLengthf);
			nIntensityNeighf = sOne_Orig_Imagef->nPixelArr[nIndexCurSizeOfANeighborf];

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)
			{
						nSumWidf -= 2 * nIntensityNeighf;
			}  // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == false)

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) 
			{
						nSumWidf -= 10 * nIntensityNeighf;
			} // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) 

			if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) 
			{

			//fprintf(fout, "\n 8: nIntensityNeighf = %d, nSumLenf = %d, nSumWidf = %d, iLenf = %d", nIntensityNeighf, nSumLenf, nSumWidf, iLenf);

			///////////////////////////////////////////////////////////////////////////////////
				nSumLenf = (int)((float)(nSumLenf) / sParameters_Weighted_Gray_3x3f->fWeight_For_2nd_Versionf); //fWeight_For_2nd_Version);
				nSumWidf = (int)((float)(nSumWidf) / sParameters_Weighted_Gray_3x3f->fWeight_For_2nd_Versionf); // fWeight_For_2nd_Version);

			} // if (sParameters_Weighted_Gray_3x3f->bVERSION_2_OF_SOBEL_3x3f == true) 
			/////////////////////////////////////////////////////////////////////

			if (sParameters_Weighted_Gray_3x3f->bUSE_SUMS_OF_IMAGE_GRADIENTSf == true) {

						//The sum of absolute values
						if (nSumLenf < 0)
						{
							nSumOfABS_Of_nSumLenf_and_nSumWidf += (-nSumLenf);
						} //if (nSumLenf < 0)
						else if (nSumLenf > 0)
						{

							nSumOfABS_Of_nSumLenf_and_nSumWidf += nSumLenf;
						} //else if (nSumLenf > 0)

						if (nSumWidf < 0)
						{
							nSumOfABS_Of_nSumLenf_and_nSumWidf += (-nSumWidf);
						} //if (nSumWidf < 0)
						else if (nSumWidf > 0)
						{

							nSumOfABS_Of_nSumLenf_and_nSumWidf += nSumWidf;
						} //else if (nSumWidf > 0)

			if (sParameters_Weighted_Gray_3x3f->bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true) {
						
						if (nSumOfABS_Of_nSumLenf_and_nSumWidf > sParameters_Weighted_Gray_3x3f->nIntensityThresholdForSobelMaxf)
							nSumOfABS_Of_nSumLenf_and_nSumWidf = nIntensityStatMax;

						if (nSumOfABS_Of_nSumLenf_and_nSumWidf < sParameters_Weighted_Gray_3x3f->nIntensityThresholdForSobelMinf)
							nSumOfABS_Of_nSumLenf_and_nSumWidf = 0;
			} //if (sParameters_Weighted_Gray_3x3f->bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true) {

						sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nSumOfABS_Of_nSumLenf_and_nSumWidf;

			} // if (sParameters_Weighted_Gray_3x3f->bUSE_SUMS_OF_IMAGE_GRADIENTSf == true) 

			////////////////////////////////////////////////////////////////////////////////////////////////////////////

			if (sParameters_Weighted_Gray_3x3f->bUSE_SUMS_OF_IMAGE_GRADIENTSf == false) {
						sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = (int)(sqrt((float)(nSumLenf*nSumLenf + nSumWidf*nSumWidf) / sParameters_Weighted_Gray_3x3f->fNormalizingf));
						//sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = (int)(sqrt( (float)(nSumLenf*nSumLenf + nSumWidf*nSumWidf) ));


				if (sParameters_Weighted_Gray_3x3f->bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true) {
							if (sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] >= sParameters_Weighted_Gray_3x3f->nIntensityThresholdForSobelMaxf)
								sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = 0;
							else
								sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = 255;
				} //if (sParameters_Weighted_Gray_3x3f->bUSING_ONLY_BLACK_AND_WHITE_COLORSf == true) {

			} // if (sParameters_Weighted_Gray_3x3f->bUSE_SUMS_OF_IMAGE_GRADIENTSf == false) 

			// The histogram threshold could be applied here
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] > nIntensityStatMax)
				sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax;
			else if (sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] < 0)
				sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = 0;

			if (sParameters_Weighted_Gray_3x3f->bBLACK_BACKGROUND_WHITE_CONTOURSf == false)
			{
				sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef] = nIntensityStatMax - sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef];
			} //if (sParameters_Weighted_Gray_3x3f->bBLACK_BACKGROUND_WHITE_CONTOURSf == false)


			//fprintf(fout, "\n iWidf = %d, iLenf = %d, sOne_Filtered_Imagef->nPixelArr[%d] = %d", iWidf, iLenf, nIndexCurSizef, sOne_Filtered_Imagef->nPixelArr[nIndexCurSizef]);

		} //for (iLenf = 0; iLenf < sOne_Filtered_Imagef->nLenCur; iLenf++)

	} //for (iWidf = 0; iWidf < sOne_Filtered_Imagef->nWidCur; iWidf++)

//#endif // #ifdef DIM3x3_SOBEL_FILTER

	  //////////////////////////////////////////////////////////////////////////////////////

	return 1;
} //int Sobel_Filtering(...

 
  //printf("\n\n Please press any key:"); getchar();
